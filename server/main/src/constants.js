const ALGORITHMS = {
  debunk: {
    name: 'DEBuNk',
    version: 1,
    lastModified: new Date('2019-02-08T08:51:35')
  },
  krippendorff: {
    name: 'krippendorff',
    version: 1,
    lastModified: new Date('2019-03-29T17:38:14')
  }
}

const APP_NAMESPACE = '67932ffd-66c7-4b29-81fc-fafab3a347a1'

const DATASETS_DIR = '/app/config/datasets'
const EXAMPLES_DIR = '/app/config/examples'

module.exports = {
  ALGORITHMS,
  APP_NAMESPACE,
  DATASETS_DIR,
  EXAMPLES_DIR
}
