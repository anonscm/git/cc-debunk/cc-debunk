const bodyParser = require('body-parser');
const urlencodedparser = bodyParser.urlencoded({
  extended: false
})
const express = require('express')
const http = require('http')
const xmlparser = require('express-xml-bodyparser')
const fs = require('fs-extra')
const child_process = require('child_process')
const path = require('path')
const csv = require('csv')
const fastcsv = require('fast-csv')
const firstline = require('firstline')
const readline = require('readline')
const cors = require('cors')
const compression = require('compression')
const util = require('util')
const uuidv5 = require('uuid/v5')
const isUUID = require('validator/lib/isUUID')
const _ = require('lodash/fp')
const WebSocket = require('ws')

const constants = require('./constants.js')
const transparency = require('./transparency.js')

const exec = util.promisify(child_process.exec)

const config = JSON.parse(fs.readFileSync("/app/config/server.config.json"))

const app = express()
app.use(cors({
  credentials: true,
  origin: true
}))

app.use(compression())

app.use(bodyParser.json()) // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})) // support encoded bodies

function readCSV(filename) {
  return new Promise((resolve, reject) => {
    fs.createReadStream(filename).pipe(csv.parse({
      delimiter: '\t'
    }, function (err, data) {
      resolve(data)
    }))
  })
}

const server = http.createServer(app)
const wss = new WebSocket.Server({ server })
server.listen(8080, function () {
  console.log("Node server running on http://localhost:8080")
})

const datasets = config.datasets.map(dataset => ({
  ...dataset,
  objects_file: path.resolve(constants.DATASETS_DIR, dataset.objects_file),
  individuals_file: path.resolve(constants.DATASETS_DIR, dataset.individuals_file),
  reviews_file: path.resolve(constants.DATASETS_DIR, dataset.reviews_file),
  selectableContextColumns: dataset.selectable_objects_attributes,
  selectableUserColumns: dataset.selectable_individuals_attributes
}))

app.get('/datasets', function (req, res) {
  res.json(datasets.map(d => ({
    name: d.name,
    type: d.type,
    arrayHeader: d.arrayHeader,
    numericHeader: d.numericHeader,
    selectableContextColumns: d.selectableContextColumns,
    selectableUserColumns: d.selectableUserColumns,
    longName: d.long_name,
    url: d.url,
    description: d.description,
  })))
})

const examples = config.examples.map(example => ({
  name: example.name,
  file: path.resolve(constants.EXAMPLES_DIR, example.file),
  description: example.description,
}))

app.get('/examples', function (req, res) {
  res.json(examples.map(e => ({
    name: e.name,
    description: e.description,
  })));
});

app.get('/example', function (req, res) {
  const example = examples.find(e => e.name == req.query.name);
  fs.readFile(example.file, {
      encoding: 'utf-8'
    })
    .then(data => res.json(JSON.parse(data)))
});

function find_dataset(dataset) {
  return datasets.find(d => d.name == dataset.name);
}

function runDSC(settingsFile) {
  const cmd = `python ./src/main.py ${settingsFile} -q`

  return exec(cmd, {
      cwd: '/app/debunk/'
    })
    .then(function ({
      stdout,
      stderr
    }) {
      console.log(stdout);
      console.log(stderr);

      return Promise.resolve()
    })
}

function runKripendorf(settingsFile) {
  const cmd = `python ./AlphaKripendorfAndBootstraping.py ${settingsFile} -q --using_approxmations --compute_p_value`

  return exec(cmd, {
    cwd: '/app/kripendorf/'
  })
    .then(function ({
      stdout,
      stderr
    }) {
      console.log(stdout);
      console.log(stderr);

      return Promise.resolve()
    })
}

function generateSentences(resultsFile, resultsDestination) {
  const pythonOutputDir = path.resolve(resultsDestination, 'python_output')
  const sentencesOutputDir = path.resolve(resultsDestination, 'sentences_output')

  return fs.mkdir(pythonOutputDir)
    .then(() => fs.mkdir(sentencesOutputDir))
    .then(() => {
      const cmd = `./sentences/bin/sentences python3 ${resultsFile} ${pythonOutputDir} ${sentencesOutputDir} data`
      console.log(cmd)
      return exec(cmd, {
        cwd: path.resolve('/app/sentences')
      })
    })
}

app.post('/getdataset', urlencodedparser, function (req, res) {
  req.setEncoding('utf8');
  const file = req.body.file;
  const dataset = req.body.dataset;

  const filename = find_dataset(dataset)[file];

  if (!filename) {
    res.status(400).send('No filename.');
    return;
  }
  console.log('Fetching dataset from file ', filename);

  var dataset_loaded = [];

  var stream = fs.createReadStream(filename);
  var csvStream = fastcsv
    .parse({
      delimiter: '\t'
    })
    .on("data", function (data) {
      dataset_loaded.push(data);
    })
    .on("end", function () {
      console.log("done");
      res.json(dataset_loaded);
    });
  stream.pipe(csvStream);
});

app.post('/getdatasetFirstLine', urlencodedparser, function (req, res) {
  req.setEncoding('utf8');
  var filename = req.body.filename;
  console.log(filename)


  firstline(filename).then(function (data) {
    console.log(data);
    res.json([data.split('\t')]);
  });
});

app.post('/getdatasetTwoLines', urlencodedparser, function (req, res) {
  req.setEncoding('utf8');
  const file = req.body.file;
  const dataset = req.body.dataset;

  const filename = find_dataset(dataset)[file];

  if (!filename) {
    res.status(400).send('No filename.');
    return;
  }
  console.log('Fetching dataset two lines from file ', filename);

  var lineReader = readline.createInterface({
    input: fs.createReadStream(filename),
  });
  var lineCounter = 0;
  var wantedLines = [];
  lineReader.on('line', function (line) {
    lineCounter++;
    wantedLines.push(line.split('\t'));
    if (lineCounter == 2) {
      lineReader.close();
    }
  });
  lineReader.on('close', function () {
    console.log(wantedLines);
    res.json(wantedLines);
  });
});

app.get('/api/results', function (req, res) {
  const {
    userID,
    jobID
  } = req.query;

  if (!userID) {
    res.status(400).send('Missing userID parameter.');
    return;
  }

  if (!jobID) {
    res.status(400).send('Missing jobID parameter.');
    return;
  }

  try {
    fs.createReadStream(`/app/results/results-${userID}.csv`).pipe(csv.parse({
      delimiter: '\t'
    }, function (err, results) {
      fs.readFile(`/app/results/qualitative-${jobID}.json`, 'utf8', function (err, dataQualJSON) {
        const configUsed = JSON.parse(dataQualJSON)

        const transparencyData = transparency.getTransparencyData(jobID)

        if (configUsed.configType == 'intra') {
          res.json({
            results: objectifyKrippendorffResults(results),
            whatWasUsed: configUsed,
            transparencyData
          })
        } else {
          const objectifiedResults = objectifyDebunkResults(results)
          if (configUsed.dataset.type == 'euparl') {
            let sentences = []
            try {
              sentences = objectifiedResults.map(
                ({ index }) => fs.readFileSync(`/app/results/${userID}/sentences_output/motif_${index}.txt`, {
                  encoding: 'utf8'
                }))
            } catch (e) {
              console.error(e)
            }

            const transparencyHtml = transparency.renderTransparencyHtml(transparencyData, objectifiedResults)

            res.json({
              results: objectifiedResults,
              whatWasUsed: configUsed,
              sentences,
              transparencyData,
              transparencyHtml
            })
          } else {
            res.json({
              results: objectifiedResults,
              whatWasUsed: configUsed,
              transparencyData
            })
          }
        }
      });
    }));
  } catch (e) {
    throw (e)
  }
})

function objectifyKrippendorffResults (results) {
  return _.map(
    ([
      index,
      group,
      context,
      sizeGroupSupport,
      sizeContextSupport,
      nbOutcomes,
      reliabilityDefault,
      reliabilityRef,
      reliabilityContextAllIndividuals,
      reliabilityContext,
      quality,
      confidenceInterval,
      patternPValue,
      stateIntraAgreement
    ]) => ({
      index,
      group,
      context,
      sizeGroupSupport,
      sizeContextSupport,
      nbOutcomes,
      reliabilityDefault,
      reliabilityRef,
      reliabilityContextAllIndividuals,
      reliabilityContext,
      quality,
      confidenceInterval,
      patternPValue,
      stateIntraAgreement
    }),
    _.tail(results))
}

function objectifyDebunkResults (results) {
  return _.map(
    ([
      index,
      context,
      g1,
      g2,
      contextSubgroup,
      g1Subgroup,
      g2Subgroup,
      ratings,
      refSim,
      patternSim,
      quality
    ]) => ({
      index,
      context: JSON.parse(context),
      g1: JSON.parse(g1),
      g2: JSON.parse(g2),
      refSim,
      patternSim,
      quality,
    }),
    _.tail(results))
}

app.get('/api/results/:userID/:jobID/transparency', ({
  params: {
    userID, jobID
  }
}, res) => {
  const transparencyData = transparency.getTransparencyData(jobID)

  const dir = fs.mkdtempSync('/tmp/')

  fs.createReadStream(`/app/results/results-${userID}.csv`).pipe(csv.parse({
    delimiter: '\t'
  }, async (err, data) => {
    const results = transparencyData.parameters.configType === 'intra' ?
          objectifyKrippendorffResults(data) :
          objectifyDebunkResults(data)

    const args = {
      dataset: transparencyData.data.name,
      config: transparencyData.parameters,
      'expected-results': results
    }

    const html = transparency.renderTransparencyHtml(transparencyData, results)

    await Promise.all([
      fs.copyFile('/app/workflows/compare_remote_packed.cwl', `${dir}/workflow.cwl`),
      fs.writeJson(`${dir}/args.json`, args),
      fs.writeFile(`${dir}/transparency.html`, html)
    ])

    await exec('tar cjf transparency.tar.gz *', {
      cwd: dir
    })

    res.sendFile(`${dir}/transparency.tar.gz`, () => {
      fs.remove(dir)
    })
  }))
})

app.get('/api/results/:userID/:jobID/workflow', ({
  params: {
    userID, jobID
  }
}, res) => {
  res.sendFile('/app/workflows/compare_remote_packed.cwl')
})

app.get('/api/results/:userID/:jobID/args', ({
  params: {
    userID, jobID
  }
}, res) => {
  const transparencyData = transparency.getTransparencyData(jobID)

  const dir = fs.mkdtempSync('/tmp/')

  fs.createReadStream(`/app/results/results-${userID}.csv`).pipe(csv.parse({
    delimiter: '\t'
  }, async (err, data) => {
    const results = transparencyData.parameters.configType === 'intra' ?
          objectifyKrippendorffResults(data) :
          objectifyDebunkResults(data)

    const args = {
      dataset: transparencyData.data.name,
      config: transparencyData.parameters,
      'expected-results': results
    }

    await fs.writeJson(`${dir}/args.json`, args),

    res.sendFile(`${dir}/args.json`, () => {
      fs.remove(dir)
    })
  }))
})

app.get('/api/results/:userID/:jobID/:id', async ({
  params: {
    userID,
    jobID,
    id
  }
}, res) => {
  if (!userID) {
    res.status(400).send('Missing userID parameter.');
    return;
  }

  if (!id) {
    res.status(400).send('Missing ID parameter.');
    return;
  }

  const configUsed = JSON.parse(await fs.readFile(`/app/results/qualitative-${jobID}.json`, 'utf8'))

  if (configUsed.configType == 'intra') {
    const createPath = type => `/app/results/${userID}/pattern_${type}_${id}.csv`;

    const [entities, group, groupOutcomes] = await Promise.all(
      _.map(
        _.compose([readCSV, createPath]),
        [
          'entities',
          'group',
          'group_outcomes'
        ]
      )
    )

    res.json({
      entities,
      group,
      groupOutcomes,
    })
  } else {
    const createPath = type => `/app/results/${userID}/${id.toString().padStart(4, '0')}_${type}.csv`;

    Promise.all(_.map(
      _.compose([readCSV, createPath]), [
        'entities',
        'g1',
        'g2',
        'g1_outcomes',
        'g2_outcomes'
      ])).then(([entities, g1, g2, g1_outcomes, g2_outcomes]) => {
      res.json({
        entities,
        g1,
        g2,
        g1_outcomes,
        g2_outcomes
      });
    })
  }
})

app.route('/api/user/:id/feedback')
  .get(function ({
    params: {
      id
    }
  }, res) {
    if (!isUUID(id, 5)) {
      throw new Error("Invalid user ID.")
    }

    fs.readFile(`/app/results/feedback/${id}.json`, {
        encoding: 'utf-8'
      })
      .then(data => res.json(JSON.parse(data)))
      .catch(() => {
        res.sendStatus(404)
      })
  })
  .post(function ({
    params: {
      id
    },
    body: {
      name,
      text
    }
  }, res) {
    if (!isUUID(id, 5)) {
      throw new Error("Invalid user ID.");
    }

    console.log(`Writing feedback for user ${id}...`)

    fs.mkdir('/app/results/feedback/')
      .catch((err) => {
        console.log('Feedback directory already exists.')
      })
      .then(() => {
        fs.writeFile(`/app/results/feedback/${id}.json`, JSON.stringify({
          name,
          text
        }), {
          encoding: 'utf8'
        })
      })
      .then(() => {
        res.sendStatus(201)
      })
  })

wss.on('connection', function(socket) {
  socket.on('message', function(data) {
    try {
      JSON.parse(data)
    } catch (e) {
      return
    }

    const msg = JSON.parse(data)
    console.debug(msg)

    const settings = JSON.parse(msg.data)
    const dataset = find_dataset(settings.dataset)

    settings.objects_file = dataset.objects_file
    settings.individuals_file = dataset.individuals_file
    settings.reviews_file = dataset.reviews_file

    const userID = isUUID(msg.userID + '', 5)
          ? msg.userID
          : uuidv5(Date.now().toString(), constants.APP_NAMESPACE)

    const jobID = uuidv5(Date.now().toString(), constants.APP_NAMESPACE)

    settings.results_destination = `/app/results/results-${userID}.csv`
    settings.detailed_results_destination = `/app/results/${userID}/`

    const settingsFile = `/app/results/qualitative-${jobID}.json`
    const statsFile = `/app/results/stats.csv`
    const resultsFile = settings.results_destination
    const resultsFileForGAT = resultsFile.slice(0, -4) + '_FOR_GAT' + resultsFile.slice(-4)
    const resultsFolder = settings.detailed_results_destination

    console.log('\n\n***************************************************\n\n')

    const log = (...msgs) => () => {
      console.log(...msgs);
      return Promise.resolve()
    }

    const send = (type, data) => {
      socket.send(JSON.stringify({ type, data }))
    }

    const emitProgress = msg => () => {
      send('progress', msg)
    }

    console.log('writeFile', settingsFile)
    const initialize = fs.writeFile(settingsFile, JSON.stringify(settings), {
      encoding: 'utf8'
    })
      .catch(console.error)
      .then(emitProgress('Starting...'))
      .then(() => fs.unlink(resultsFile), () => {})
      .then(() => exec(`rm -rf ${resultsFolder}/*`), () => {})
      .then(log(`Writing stats in ${statsFile}`))
      .then(() => fs.appendFile(statsFile, [jobID, Date.now(), userID, settingsFile].join('\t') + '\n'))

    if (settings.configType === 'intra') {
      initialize
        .then(log('runKripendorf', settingsFile))
        .then(emitProgress('Running'))
        .then(() => runKripendorf(settingsFile))
        .then(() => {
          fs.createReadStream(resultsFile).pipe(csv.parse({
            delimiter: '\t'
          }, function (err, results) {
            send('done', {
              userID,
              jobID
            });
          }));
        })
        .then(log('Execution completed!'))
        .catch(e => {
          console.log('Execution failed.')
          console.error(e)
          socket.emit('failed', e)
        })
    } else {
      initialize
        .then(log('runDSC', settingsFile))
        .then(emitProgress('Running'))
        .then(() => runDSC(settingsFile))
        .then(log('generateSentences', resultsFileForGAT))
        .then(dataset.type == 'euparl'
              ? () => generateSentences(resultsFileForGAT, settings.detailed_results_destination)
              : Promise.resolve())
        .then(() => {
          fs.createReadStream(resultsFile).pipe(csv.parse({
            delimiter: '\t'
          }, function (err, results) {
            send('done', {
              userID,
              jobID
            });
          }));
        })
        .then(log('Execution completed!'))
        .catch(e => {
          console.log('Execution failed.')
          console.error(e)
          send('failed', e)
        })
    }
  })
})
