const _ = require('lodash/fp')
const fs = require('fs-extra')
const pug = require('pug')

const constants = require('./constants.js')

function getTransparencyData (jobID) {
  const json = fs.readFileSync(`/app/results/qualitative-${jobID}.json`, 'utf8')
  const configUsed = JSON.parse(json)

  return {
    general: {
      date: fs.statSync(`/app/results/qualitative-${jobID}.json`).mtime
    },
    platform: {
      version: 1
    },
    data: _.pick(['name', 'longname', 'type', 'description', 'url'], configUsed.dataset),
    algorithm: configUsed.type == 'inter' ? constants.ALGORITHMS.debunk : constants.ALGORITHMS.krippendorff,
    parameters: configUsed
  }
}

function renderTransparencyHtml(data, results) {

  const renderTransparency = pug.compileFile('./src/transparency.pug')

  return renderTransparency({...data, rows: results.map(displayRow)})
}

function displayRow ({ index, context, g1, g2, refSim, patternSim, quality }) {
  return {
    index,
    pattern: {
      refSim,
      patternSim,
      style_context_bar: `width: 3px; position: absolute; left: ${percentage(refSim)};`,
      style_progress: `width: ${percentage(patternSim)}; background: rgb(${colorPercentage([221, 75, 57], [0, 166, 90], patternSim).join(',')});`,
      quality: Number.parseFloat(quality)
    },
    change: (patternSim - refSim) / refSim,
    g1,
    g2,
    context
  }
}

function percentage (val) {
  return `${_.round(_.multiply(100, val))}%`
}

function colorPercentage ([r1, g1, b1], [r2, g2, b2], percentage) {
  const percent = (a, b) => a + (b - a) * percentage
  return [percent(r1, r2), percent(g1, g2), percent(b1, b2)]
}

module.exports = {
  getTransparencyData,
  renderTransparencyHtml
}
