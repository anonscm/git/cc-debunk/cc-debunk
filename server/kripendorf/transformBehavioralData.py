import argparse
from util.csvProcessing import writeCSV,writeCSVwithHeader,readCSVwithHeader
from datetime import datetime
from filterer.filter import filter_pipeline_obj
from functools import partial
from operator import itemgetter
import gc
from util.jsonProcessing import readJSON,readJSON_stringifyUnicodes
from os.path import basename, splitext, dirname
import os

def process_outcome_dataset(itemsfile,usersfile,outcomesfile,numeric_attrs=[],array_attrs=[],outcome_attrs=None,itemsScope=[],users_1_Scope=[],users_2_Scope=[],ratings_to_remove=set(),nb_items=float('inf'),nb_individuals=float('inf'),delimiter='\t',replace_ids=False): 

	nb_itemsets_all_context=0
	nb_itemsets_all_individuals=0
	FULL_OUTCOME_CONSIDERED=True
	ITEMS_METADATA_NEEDED=False
	USE_CACHE=False
	SIZE_ESTIMATION=False
	SMALLER_DESCRIPTION_SPACE=True
	VERBOSE=False
	CLARIFY=True
	nb_outcome_considered=0


	AM_I_DEALING_WITH_PREAGGREGATED_DISTRIBUTIONS=False

	domain_of_possible_outcomes=set()
	if 'CACHE' in dir(process_outcome_dataset):
		items,items_header,users,users_header,outcomes,outcomes_header,items_id,users_id,outcome_attrs,position_attr,outcomes_processed,considered_items,users1,users2,(considered_items_ids),(considered_users_1_ids),(considered_users_2_ids),(considered_users_ids)=process_outcome_dataset.CACHE
	else:
		items,items_header=readCSVwithHeader(itemsfile,numberHeader=numeric_attrs,arrayHeader=array_attrs,selectedHeader=None,delimiter=delimiter)
		users,users_header=readCSVwithHeader(usersfile,numberHeader=numeric_attrs,arrayHeader=array_attrs,selectedHeader=None,delimiter=delimiter)
		outcomes,outcomes_header=readCSVwithHeader(outcomesfile,numberHeader=numeric_attrs,arrayHeader=array_attrs,selectedHeader=None,delimiter=delimiter)	
		items_id=items_header[0]
		users_id=users_header[0]
		
		outcome_attrs=outcome_attrs if outcome_attrs is not None else [outcomes_header[2]]
		position_attr=outcome_attrs[0]
		# outcomes_filtered=[];outcomes_filtered_append=outcomes_filtered.append
		# for row in outcomes:
		# 	if row[position_attr] not in ratings_to_remove:
		# 		outcomes_filtered_append(row)
		# outcomes=outcomes_filtered
		
		outcomes=[row for row in outcomes if row[position_attr] not in ratings_to_remove]

		
		outcomes_processed=outcomes
		
		# ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES={}
		# for row in outcomes_processed:
		# 	v_id_rev=row[items_id]
		# 	if v_id_rev not in ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES:
		# 		ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES[v_id_rev]=0
		# 	ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES[v_id_rev]+=1
		
		# set_of_items_having_pairable_outcomes=set(map(itemgetter(0),(filter(lambda x:x[1]>1,ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES.items())))) #Filtering entities having no pairable outcomes

		considered_items=filter_pipeline_obj(items, itemsScope)[0]
		#print len(considered_items),len({x[items_id] for x in considered_items})
		already_seen_ids=set()
		considered_items_final=[]
		for row in considered_items:
			if row[items_id] in already_seen_ids:
				continue
			considered_items_final.append(row)
			already_seen_ids|={row[items_id]}

		considered_items=considered_items_final
		# print len(considered_items)
		# raw_input('***')
		users1=filter_pipeline_obj(users, users_1_Scope)[0]
		users2=filter_pipeline_obj(users, users_2_Scope)[0]

		get_items_ids = partial(map,itemgetter(items_id))
		get_users_ids = partial(map,itemgetter(users_id))

		considered_items_ids=set(get_items_ids(considered_items))
		considered_users_1_ids=set(get_users_ids(users1))
		considered_users_2_ids=set(get_users_ids(users2))
		considered_users_ids=set(considered_users_1_ids)|set(considered_users_2_ids)
		if USE_CACHE:
			process_outcome_dataset.CACHE=[items,items_header,users,users_header,outcomes,outcomes_header,
										   items_id,users_id,outcome_attrs,position_attr,outcomes_processed,
										   considered_items[:],users1[:],users2[:],
										   set(considered_items_ids),set(considered_users_1_ids),set(considered_users_2_ids),set(considered_users_ids)]


	if nb_items<float('inf'):
		nb_items=int(nb_items)
		considered_items_ids=set(sorted(considered_items_ids)[:nb_items])
		considered_items=[x for x in considered_items if x[items_id] in considered_items_ids]
	if nb_individuals<float('inf'):
		nb_individuals=int(nb_individuals)
		considered_users_ids=set(sorted(considered_users_ids)[:nb_individuals])
		users1=[x for x in users1 if x[users_id] in considered_users_ids]
		users2=[x for x in users2 if x[users_id] in considered_users_ids]



	all_users_to_items_outcomes={}
	all_items_to_users_outcomes={}
	outcomes_considered=[];outcomes_considered_append=outcomes_considered.append
	items_metadata={row[items_id]:row for row in considered_items if row[items_id] in considered_items_ids} if ITEMS_METADATA_NEEDED else {}
	users_metadata={row[users_id]:row for row in users if row[users_id] in considered_users_ids}

	###############

	ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES={}
	for row in outcomes_processed:
		if row[users_id] in considered_users_ids:
			v_id_rev=row[items_id]
			if v_id_rev in considered_items_ids:

				if v_id_rev not in ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES:
					ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES[v_id_rev]=0
				ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES[v_id_rev]+=1
	set_of_items_having_pairable_outcomes=set(map(itemgetter(0),(filter(lambda x:x[1]>1,ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES.items()))))
	considered_items_ids=considered_items_ids&set_of_items_having_pairable_outcomes
	###############


	for row in outcomes_processed:
		v_id_rev=row[items_id]
		u_id_rev=row[users_id]
		if v_id_rev in considered_items_ids and u_id_rev in considered_users_ids:
			pos_rev=row[position_attr]
			
			if u_id_rev not in all_users_to_items_outcomes:
				all_users_to_items_outcomes[u_id_rev]={}
			if v_id_rev not in all_items_to_users_outcomes:
				all_items_to_users_outcomes[v_id_rev]={}

			all_users_to_items_outcomes[u_id_rev][v_id_rev]=pos_rev
			all_items_to_users_outcomes[v_id_rev][u_id_rev]=pos_rev
			domain_of_possible_outcomes|={pos_rev}
			if FULL_OUTCOME_CONSIDERED: outcomes_considered_append({items_id:v_id_rev,users_id:u_id_rev,position_attr:pos_rev})
			nb_outcome_considered+=1

	considered_users_1_sorted=sorted(users1,key=itemgetter(users_id))
	considered_users_2_sorted=sorted(users2,key=itemgetter(users_id))
	considered_items_sorted=sorted(considered_items,key=itemgetter(items_id))
	considered_items_sorted=[row for row in considered_items_sorted if row[items_id] in considered_items_ids]

	if SIZE_ESTIMATION:
		from pympler.asizeof import asizeof
		print asizeof(all_users_to_items_outcomes)
		print asizeof(considered_items_sorted)
		print asizeof(considered_users_1_sorted)
		print asizeof(considered_users_2_sorted)
	gc.collect()


	



	REPLACING_IDS=replace_ids
	if REPLACING_IDS:
		#print len(considered_items_ids),len(considered_items_sorted)
		#raw_input('xxx')
		ind_to_dict_items={i:x[items_id] for i,x in enumerate(considered_items_sorted)}
		dict_to_ind_items={v:k for k,v in ind_to_dict_items.iteritems()}
		considered_items_sorted=[dict([(items_id,dict_to_ind_items[x[items_id]])]+[(k,v) for k,v in x.items() if k !=  items_id]) for x in considered_items_sorted] 
		#print len(ind_to_dict_items),len(dict_to_ind_items)
		#raw_input('ccccccccccccc')
		


		ind_to_dict_users={i:x for i,x in enumerate(users_metadata)}
		dict_to_ind_users={v:k for k,v in ind_to_dict_users.iteritems()}
		considered_users_1_sorted=[dict([(users_id,dict_to_ind_users[x[users_id]])]+[(k,v) for k,v in x.items() if k !=  users_id]) for x in considered_users_1_sorted]
		considered_users_2_sorted=[dict([(users_id,dict_to_ind_users[x[users_id]])]+[(k,v) for k,v in x.items() if k !=  users_id]) for x in considered_users_2_sorted]
		considered_users_sorted=[dict([(users_id,dict_to_ind_users[x[users_id]])]+[(k,v) for k,v in x.items() if k !=  users_id]) for x in considered_users_sorted]
		all_users_to_items_outcomes={dict_to_ind_users[u]:{dict_to_ind_items[v]:o for v,o in u_votes.iteritems()} for u,u_votes in all_users_to_items_outcomes.iteritems()}
		all_items_to_users_outcomes={dict_to_ind_items[v]:{dict_to_ind_users[u]:o for u,o in v_votes.iteritems()} for v,v_votes in all_items_to_users_outcomes.iteritems()}
		items_metadata={row[items_id]:row for row in considered_items_sorted}
		users_metadata={dict_to_ind_users[u]:dict([(users_id,dict_to_ind_users[x[users_id]])]+[(k,v) for k,v in x.items() if k !=  users_id]) for u,x in users_metadata.iteritems()}
		
			

		considered_items_sorted=sorted(considered_items_sorted,key=lambda x:x[items_id])
		considered_users_1_sorted=sorted(considered_users_1_sorted,key=lambda x:x[users_id])
		considered_users_2_sorted=sorted(considered_users_2_sorted,key=lambda x:x[users_id])
		# print len(all_items_to_users_outcomes[4231]),ind_to_dict_items[4231],ITEMS_COUNT_INDIVIDUALS_GIVEN_OUTCOMES[ind_to_dict_items[4231]]
		# raw_input('....')

	

	# if AM_I_DEALING_WITH_PREAGGREGATED_DISTRIBUTIONS:
	# 	nb_outcome_considered=0.
		
	# 	for u in all_users_to_items_outcomes:
	# 		for e in all_users_to_items_outcomes[u]:
	# 			all_users_to_items_outcomes[u][e]=eval(all_users_to_items_outcomes[u][e])
	# 			all_items_to_users_outcomes[e][u]=eval(all_items_to_users_outcomes[e][u])
	# 			nb_outcome_considered+=sum(all_users_to_items_outcomes[u][e])
	# 	nb_outcome_considered=int(nb_outcome_considered)
	# 	print "nb_outcome_considered : ",nb_outcome_considered
	# 	domain_of_possible_outcomes=[iscore for iscore in range(len(all_users_to_items_outcomes[all_users_to_items_outcomes.keys()[0]][all_users_to_items_outcomes[all_users_to_items_outcomes.keys()[0]].keys()[0]]))]
	# 	print "domain_of_possible_outcomes : ",domain_of_possible_outcomes
	# 	for u in all_users_to_items_outcomes:
	# 		for e in all_users_to_items_outcomes[u]:
	# 			print u,e,all_users_to_items_outcomes[u][e]
	# 			raw_input('....')

	process_outcome_dataset.STATS={
		'nb_items_entities':nb_itemsets_all_context,
		'nb_items_individuals':nb_itemsets_all_individuals,
		'items_header':items_header,
		'users_header':users_header,
		'outcomes_header':outcomes_header

	}
	#return items_metadata,users_metadata,all_users_to_items_outcomes,all_items_to_users_outcomes,domain_of_possible_outcomes,outcomes_considered,items_id,users_id,considered_items_sorted,considered_users_1_sorted,considered_users_2_sorted,nb_outcome_considered
	return considered_items_sorted,considered_users_1_sorted,outcomes_considered







if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='TransformBehavioralData')
	parser.add_argument('file', metavar='ConfigurationFile', type=str,  help='the input configuration file')
	args=parser.parse_args()


	json_file_path=args.file
	json_config=readJSON_stringifyUnicodes(json_file_path)


	
	entities_file_path=json_config["entities_file"]
	individuals_file_path=json_config["individuals_file"]
	outcomes_file=json_config["reviews_file"]
	numbers_header=json_config["numericHeader"]
	itemsScope=json_config["entities_scope"]
	inidividuals_scope=json_config["individuals_scope"]
	
	ratings_to_remove=json_config["ratings_to_remove"]
	delimiter=json_config["delimiter"]
	nb_entities=json_config["nb_entities"]
	nb_individuals=json_config["nb_individuals"]


	
	considered_items_sorted,considered_users_1_sorted,outcomes_considered =process_outcome_dataset(entities_file_path,individuals_file_path,outcomes_file,numeric_attrs=numbers_header,array_attrs=[],outcome_attrs=None,itemsScope=itemsScope,users_1_Scope=inidividuals_scope,users_2_Scope=inidividuals_scope,ratings_to_remove=ratings_to_remove,delimiter=delimiter,nb_items=nb_entities,nb_individuals=nb_individuals)
	

	items_header=process_outcome_dataset.STATS["items_header"]
	users_header=process_outcome_dataset.STATS["users_header"]
	outcomes_header=process_outcome_dataset.STATS["outcomes_header"]


	

	fileparent = dirname(json_config["entities_file_output"])

	if not os.path.exists(fileparent):
		os.makedirs(fileparent)
	

	writeCSVwithHeader(considered_items_sorted,json_config["entities_file_output"],items_header,delimiter='\t')
	writeCSVwithHeader(considered_users_1_sorted,json_config["individuals_file_output"],users_header,delimiter='\t')
	writeCSVwithHeader(outcomes_considered,json_config["reviews_file_output"],outcomes_header,delimiter='\t')




