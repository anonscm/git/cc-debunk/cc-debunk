# -*- coding: utf-8 -*-
from nltk import FeatStruct
from nltk import FeatDict



"""
	Sert à transformer la sortie de la plannificaiton en arbre récupérable par la réalisation de surface
"""

def DoctoSimpleNLG(documentSpec):
	#A partir d'un ensemble de prototype de phrase, fait la grammaticalisation de chacune

	specs=[]
	for p in document:
		for sent in p:
			specs.append(toSimpleNLGPhraseSpec(sent))
	return specs

def PPSAbstractSyntaxtoSimpleNLGPhraseSpec(sent):
	s=""
	s +="sentence("
	if 'frontmodifier' in sent:
		s+= 'fm(' + toSimpleNLGPhraseSpec(sent['frontmodifier']) + ')'+','
	if 'subject' in sent:
		s+= toSimpleNLGPhraseSpec(sent['subject'])+','
	if 'head' in sent:
		s+='vp(lex='+sent['head']+','
		if 'features' in sent:
			for key in sent['features']:
				s+=key+'='+sent['features'][key]+','
		s=s[:len(s)-1]+'),'
	if 'object' in sent:
		s+= toSimpleNLGPhraseSpec(sent['object'])+','
	if 'complement' in sent:
		s+= toSimpleNLGPhraseSpec(sent['complement'])+','
	if 'ponctuation' in sent:
		s+= 'ponctuation('+sent['ponctuation']+'),'
	s=s[:len(s)-1]+')'
	return s

def ReferringNPtoSimpleNLGPhraseSpec(sent):
	s=""
	feats=""
	lex=""
	s +="np("
	if 'head' in sent:
		lex=sent['head']
		for key in sent['features']:
				if key == 'definite':
					continue
				feats+=key+'='+sent['features'][key]+','

	if 'modifier' in sent:
		feats+='modifier='+str(sent['modifier'])+','
	if 'premodifier' in sent:
		for elt in sent['premodifier']:
			s+='premodifier=' + str(sent['premodifier'][elt]) + ','
	s+='lex='+str(lex)+','+str(feats)
	if 'complement' in sent:
		for elt in sent['complement']:
			s+='complement=' + toSimpleNLGPhraseSpec(sent['complement'][elt]) + ','
	s=s[:len(s)-1]+')'
	return s

def PSCannedTexttoSimpleNLGPhraseSpec(sent):
	s=""
	s +="string("
	if 'text' in sent:
		s+='lex='+str(sent['text'])+','
	s=s[:len(s)-1]+')'
	return s

def AggregationtoSimpleNLGPhraseSpec(sent):
	s=""
	s +="aggregation("
	if 'conjonction' in sent:
		s+= "and,"
	if 'members' in sent:
		for member in sent ['members']:
			s+= toSimpleNLGPhraseSpec(member) + ','
	s=s[:len(s)-1]+')'
	return s

def toSimpleNLGPhraseSpec(sent):
	s=""
	if type(sent) == FeatDict:
		if sent['type']=='PPSAbstractSyntax':
			s+= PPSAbstractSyntaxtoSimpleNLGPhraseSpec(sent)
		elif sent['type']=='ReferringNP':
			s+= ReferringNPtoSimpleNLGPhraseSpec(sent)
		elif sent['type']=='PSCannedText':
			s+= PSCannedTexttoSimpleNLGPhraseSpec(sent)
	return s
