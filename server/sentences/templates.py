from nltk import FeatStruct

def initTemplates():
    """
    Création des templates des phrases à génerer
    Chaque élément de templates est une liste, pour pouvoir avoir plusieurs possibilité de template pour une même phrase
    """

    templates = {}
    templates['motif_subjects_intro'] = [FeatStruct("[type = PPSAbstractSyntax, head='be', features=[tense='lexicalizeTense(tense)'], subject=[type=ReferringNP, head='the subject', features=[number='lexicalizeNum2Agreement(nb_subject)'], premodifier=[value0='lexicalizeChange(change)']], ponctuation=colon]")]
    templates['motif_subject'] = [FeatStruct("[type=PSCannedText, text='lexicalizeCannedString("'"    - {}"'",lexicalize(description))']")]
    templates['statistics'] = [FeatStruct("[type=PPSAbstractSyntax, head='bring', features=[tense='lexicalizeTense(tense)'], subject=[type=ReferringNP, head='the group', premodifier=[value0='two'], features=[number='pl']], complement=[type = PSCannedText, text='lexicalizeCannedString("'"together {} and {} individuals respectively."'",lexicalize(g1_size),lexicalize(g2_size))']]")]
    templates['global_similarity'] = [FeatStruct("[type=PPSAbstractSyntax, head='be', features=[tense='lexicalizeTense(tense)'], subject=[type=ReferringNP, head='the group', premodifier=[value0='two'], features=[number='pl']], complement=[type=PSCannedText, text='lexicalizeCannedString("'"{}"'",lexicalizeAgreement(sim_tot))'], frontmodifier=[type=PSCannedText, text='lexicalizeCannedString("'"In general terms"'")']]")]
    templates['change_values'] = [FeatStruct("[type=PPSAbstractSyntax, head='witness', features=[tense='lexicalizeTense(tense)', passive=true], subject=[type=ReferringNP, head='this', features=[]], complement=[type=PSCannedText, text='lexicalizeCannedString("'"by {} of similar majority vote from {}% to {}%"'",lexicalizeSimilarityDifference(sim_diff),lexicalize(sim_tot),lexicalize(sim_rel))']]")]

    return templates


def buildTemplates(templates, booltab, booltab1, booltab2):
    """
    Fonction pour générer des tempaltes en fonction des données présentes
    """

    templates['group_description1'] = [FeatStruct(buildGroupTemplate(booltab1))]
    templates['group_description2'] = [FeatStruct(buildGroupTemplate(booltab2))]
    templates['change'] = [FeatStruct(buildDateTemplate(booltab))]
    return templates


def buildDateTemplate(booltab):
    """
    Génére les templates en fonction de la présence ou non d'une limite temporelle des votes
    """

    base = "[type=PPSAbstractSyntax, head='observe', features=[tense='lexicalizeTense(tense)'], subject=[type=ReferringNP, head='we', features=[]], complement=[type=PSCannedText, text='lexicalizeCannedString("'"a {} between majorities of the two groups"'",lexicalizeAgreementChange(sim_rel))'], frontmodifier=[type=PSCannedText, text="
    without_date = "'lexicalizeCannedString("'"But on {} ballots"'",lexicalize(ballots_number))']]"
    one_month = "'lexicalizeCannedString("'"But on {} ballots cast in {}"'",lexicalize(ballots_number),lexicalizeDate(date))']]"
    period = "'lexicalizeCannedString("'"But on {} ballots cast between {} and {}"'",lexicalize(ballots_number),lexicalizeDate(date1),lexicalizeDate(date2))']]"
    if (booltab[1]):
        if (booltab[2]):
            return base + period
        else:
            return base + one_month
    else:
        return base + without_date


def buildGroupTemplate(booltab):
    """
    Génére les templates de description de groupe d'individus en fonction des attributs commun des membres
    """

    base = "[type=PPSAbstractSyntax,head='gather',features=[tense='lexicalizeTense(tense)'],subject=[type=ReferringNP,head='the group',premodifier=[value0='lexicalize(group_number)'],features=[]],object=[type=ReferringNP,head='MEP',features=[number='pl']"
    t1, tmp = base, ""
    PreMod, Comp = False, False

    if (booltabEmpty(booltab)):
        t1 += ",premodifier=[value0=all]]]"
    else:
        #Pre-modifiers
        if (booltab[0] and not booltab[2] and not booltab[4]):
            t1 += ",premodifier=[value0='lexicalizeParty(EPG)']"
        elif (booltab[1] and not booltab[4]):
            t1 += ",premodifier=[value1='lexicalizeParty(NP)']"
        else:
            if (booltab[2]):
                tmp += "value2='lexicalizeNationality(Country)',"
                PreMod = True
            if (booltab[4]):
                tmp += "value4='lexicalizeGender(Gender)',"
                PreMod = True
            if (PreMod):
                tmp = tmp[:len(tmp)-1]
                t1 += ",premodifier=[" + tmp + "]"
        tmp=""
        #Complement
        if (booltab[0] and (booltab[2] or booltab[4])):
            tmp+="value0=[type=PSCannedText, text='lexicalizeCannedString("'"from the {}"'",lexicalizeParty(EPG))'],"
            Comp = True
        if (booltab[1] and booltab[4]):
            tmp+="value1=[type=PSCannedText, text='lexicalizeCannedString("'"from the {}"'",lexicalizeParty(NP))'],"
            Comp = True
        if (booltab[3]):
            if (booltab[6]):
                tmp+="value3=[type=PSCannedText,text='lexicalizeCannedString("'"born between {} and {}"'",lexicalizeYear(date1),lexicalizeYear(date2))'],"
            else:
                tmp+="value3=[type=PSCannedText,text='lexicalizeCannedString("'"born in {}"'",lexicalizeYear(date))'],"
            Comp = True
        if (Comp):
            tmp = tmp[:len(tmp)-1]
            t1 += ",complement=[" + tmp + "]"
        t1 += "]]"
    return t1;

def booltabEmpty(booltab):
    for elt in booltab:
        if (elt):
            return False
    else:
        return True
