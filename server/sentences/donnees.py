# -*- coding: utf-8 -*-

import csv


class ParamNotHandled(Exception):
    """
    Paramètre pas encore pris en compte par le programme
    """
    pass

def readCSVFile(filename):
    """
        ouverture du fichier csv pour le mettre sous forme de dictionnaire ordonné
    """
    csvfile = open(filename, newline='', encoding="utf-8")
    reader = csv.DictReader(csvfile, delimiter='\t')
    return reader

def stringToStringTab(str):
    """
        fonction pour récupérer le contenu des données du contexte
    """

    tab, tmp, i = [], "", 0
    while (i < len(str)):
        if (str[i] == "'"):
            j, i = i+1, i+1
            while (str[i] != "'"):
                i += 1
            tab.append(str[j:i])
        if (str[i] == '"'):
            j, i = i+1, i+1
            while (str[i] != '"'):
                i += 1
            tab.append(str[j:i])
        i += 1
    return tab


def contextAnalysis(meta, data):
    """
        mets le contexte dans un format adéquat
    """

    context = {'subject' : [], 'date' : []}
    booltab = [False, False, False]
    for (elem,value) in zip(meta,data):
        if (elem == 'subject'):
            context['subject'] = value
            booltab[0] = True
        elif (elem == 'date'):
            context['date'] = value
            booltab[1] = True
            if (context['date'][0] == context['date'][1]):
                pass
            else:
                booltab[2] = True
        else:
            print("[ERROR] ATTRIBUT DE CONTEXTE NON PRIS EN COMPTE : " + elem)
    return (context, booltab)



def groupAnalysis(meta, data):
    """
        analyse les caractéristiques d'un groupe d'individus et les renvoie dans un format adéquat
    """

    group = {'EP_Group': "", 'NatParty': "", 'Country': "", 'Age': [], 'Gender': "", 'name': "", }
    booltab = [False, False, False, False, False, False, False]
    for i in range(0, len(meta)):
        if (meta[i] == "EPG"):
            group['EP_Group'] = data[i]
            booltab[0] = True
        elif (meta[i] == "NP"):
            group['NatParty'] = data[i]
            booltab[1] = True
        elif (meta[i] == "Country"):
            group['Country'] = data[i]
            booltab[2] = True
        elif (meta[i] == "Age"):
            group['Age'] = data[i].strip('[').strip(']').split(',')
            if (int(group['Age'][0]) == int(group['Age'][1])):
                pass
            else:
                booltab[6] = True
            booltab[3] = True
        elif (meta[i] == "Gender"):
            group['Gender'] = data[i]
            booltab[4] = True
        elif (meta[i] == "Individual"):
            group['name'] = data[i]
            booltab[5] = True
        elif (meta[i] == ""):
            pass
        else:
            print("[ERROR] ATTRIBUT DE GROUPE NON PRIS EN COMPTE : " + meta[i])

    if (booltab[0] and booltab[1]):
        booltab[0] = False
    if (booltab[1] and booltab[2]):
        booltab[2] = False
    return (group, booltab)


def getResults(filename):
    """
        récupère les données du fichiers de résultats ligne par ligne
    """

    reader = readCSVFile(filename)
    data, results_line = [], {}
    databool, databool1, databool2 = [], [], []

    for e in reader:
        #g1':groupAnalysis(e['g1']), 'g2':groupAnalysis(e['g2']),
        results_line={}
        results_line['ind'] = e['ind']
        results_line['context_size'] = int(e['|subgroup(context)|'])
        results_line['g1_size'] = int(e['|subgroup(g1)|'])
        results_line['g2_size'] = int(e['|subgroup(g2)|'])
        results_line['reviews_size'] = int(e['|ratings(c,g1,g2)|'])
        #on considère que les similarité n'ont que 2 chiffres après la virgule
        results_line['ref_sim'] = int(float(e['ref_sim']) * 100)
        results_line['pattern_sim'] = int(float(e['pattern_sim']) * 100)
        results_line['quality'] = int(float(e['quality']) * 100)

        #CONTEXT
        meta_context = eval(e['meta_context'])
        data_context = eval(e['context'])
        (context, booltab) = contextAnalysis(meta_context, data_context)
        results_line['subject'] = context['subject']
        results_line['date'] = context['date']

        #GROUP1
        meta_g1 = eval(e['meta_g1'])
        g1 = eval(e['g1'])
        (group1, booltab1) = groupAnalysis(meta_g1, g1)
        results_line['g1_EP_Group'] = group1['EP_Group']
        results_line['g1_NatParty'] = group1['NatParty']
        results_line['g1_Country'] = group1['Country']
        results_line['g1_Age'] = group1['Age']
        results_line['g1_Gender'] = group1['Gender']
        results_line['g1_name'] = group1['name']

        #GROUP2
        meta_g2 = eval(e['meta_g2'])
        g2 = eval(e['g2'])
        (group2, booltab2) = groupAnalysis(meta_g2, g2)
        results_line['g2_EP_Group'] = group2['EP_Group']
        results_line['g2_NatParty'] = group2['NatParty']
        results_line['g2_Country'] = group2['Country']
        results_line['g2_Age'] = group2['Age']
        results_line['g2_Gender'] = group2['Gender']
        results_line['g2_name'] = group2['name']


        #attention la ligne d'indice 1 sera en position 0
        data.append(results_line)
        databool.append(booltab)
        databool1.append(booltab1)
        databool2.append(booltab2)
    return (data, databool, databool1, databool2)
