# -*- coding: utf-8 -*-

import csv

"""
    Sert à la lexicalisation des sujets de vote au parlement européen, en mettant les sujets sous forme d'arbre à parti d'un ficheir csv.
"""

class NoeudTag:
    """
    cette classe sert à construire l'arbre des sujets de votes au parlement européen.
    """
    tag = None
    description = ""
    Fils = []

    def __init__(self, tag, description):
        self.tag = tag
        self.fils = []
        self.description = description

    def toString(self):
        print("{\n\ttag : " + self.tag)
        print("\n\tdescription : " + self.description)
        print("\n\tfils : ")
        print(self.fils)
        print("}\n")


def searchTab(tab, tag):
    for elt in tab:
        if (elt.tag == tag):
            return elt
    print("ERREUR : Tag non trouvé")


def searchNod(tabTag, tree):
    cour = {}
    if (tabTag == []):
        cour = tree
    else:
        cour = tree
        for i in range(0, len(tabTag)):
            cour = searchTab(cour.fils, tabTag[i])
    return cour


def initTree(filename):
    csvfile = open(filename, newline='', encoding="utf-8-sig")
    reader = csv.DictReader(csvfile, delimiter='\t')

    #racine
    tree = NoeudTag('-1',"")

    #remplissage global
    for elt in reader:
        tab = elt['TAG_ID'].split('.')
        pere = searchNod(tab[:len(tab) - 1], tree)
        pere.fils += [NoeudTag(tab[len(tab) - 1].strip(), elt['TAG_LABEL'].strip(' '))]

    return tree


def tagToDescription(tag, tree):
    tab = tag.split('.')
    node = searchNod(tab, tree)
    return node.description.strip('\n')
