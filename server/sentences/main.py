#!/usr/bin/python3
# -*- coding: utf-8 -*-

from donnees import *
from dico_tag import *
#from donnees_users import *
from macro_planning import *
from templates import *
from micro_planning import *
from gramm_planning import *

import sys
import getopt
import os
import sys
from nltk import FeatStruct
"""
Entrée :
- un fichier csv contenant les résultats d'une analyse
- des fichiers csv contenant des données de lexicalisation

Sortie : un texte explicatif
"""


def main(filename, output_dir, data_path):
    """
    Fonction principale du programme de génération.
    Récupère les données et lance la génération de texte
    """

    (results, databool, databool1, databool2) = getResults(filename)
    templates = initTemplates()

    #GAT sur les motifs
    for i in range(0, len(results)):
        #Macro Plannification
        motifPlan = toMotifPlan(results[i], databool[i], databool1[i],
                                databool2[i])
        templates = buildTemplates(templates, databool[i], databool1[i],
                                   databool2[i])
        #Micro Plannification

        phrasespecs = toProtoPhrase(templates, motifPlan, data_path)

        #On écrit le texte généré dans un ficheir txt

        name = output_dir + "/motif_" + results[i]['ind']
        output_file = open(name, 'wb')

        for p in phrasespecs:
            for sent in p:
                output_file.write(toSimpleNLGPhraseSpec(sent).encode('utf-8') + b'\n')
            output_file.write(b'\n')
        output_file.close()


if __name__ == "__main__":
    [input_file, output_dir, data_path] = sys.argv[1:]
    main(input_file, output_dir, data_path)
