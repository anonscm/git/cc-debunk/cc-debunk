# -*- coding: utf-8 -*-
from nltk import FeatStruct
from nltk import FeatDict

from EPG import *
from CountryNationality import *
from NationalParty import *

from math import *


def getValueInMessage(attribut, message):
    """
		retourne la valeur indiquée par la clé attribut dans le dictionnaire message
		retourne None si l'attribut n'existe pas ou si message n'est pas un dictionnaire
	"""
    if isinstance(message, dict):
        for key in message:
            if key == attribut:
                return message[key]
            val = getValueInMessage(attribut, message[key])
            if val != None:
                return val
    return None


class Lexicalize:
    """
		Classe permettant d'effectuer la lexicalisation des valeurs d'attribut des messages
		Cette classe utilise le mécanisme de reflexion (http://www.diveintopython.net/power_of_introspection/getattr.html)
		Selon la chaine de caractere (e.g., 'lexicalizeIntro') on appelle la fonction de ce nom si elle existe. Attention toutes les fonctions lexicalizeXX doivent prendre les arguments dans le meme ordre
	"""

    def lexicalize(self, message, value):

        # si la valeur de base à lexicaliser n'est pas fonctionelle
        if not str(value).startswith("lexicalize"):
            val = getValueInMessage(value, message)
            if val != None:
                return val
            return value

        #si c'est fonctionnel on verifie la syntaxe
        if not value.endswith(')'):
            print("syntax error in %s missing ending ')'\n" % (value))
            exit()
        #on prepare l'appel par reflexion
        function_name, args = value[0:len(value) - 1].split('(', 1)
        function = getattr(self, function_name)
        #pas de fonction de ce nom là
        if function == None:
            print("function " + str(function_name) + " does not exist")
            return value

        args_list = [message]
        for arg in args.split(','):
            args_list.append(self.lexicalize(message, arg))

        #appel de la fonction
        results = (function(*args_list))
        return results

    def lexicalizeCannedString(self, message, chaine, *args):
        """
			Considère que le la chaine 'chaine' doit être formatée avec les arguments 'args'
		"""
        args_list = []
        for arg in args:
            args_list.append(self.lexicalize(message, arg))
        return chaine.format(*args_list)

    def lexicalizeChange(self, message, change):
        """
			renvoie si les sujets sont consensuels ou confilctuels
		"""
        if (change < 0):
            return "consensual"
        else:
            return "conflictual"

    def lexicalizeNum2Agreement(self, message, nombre):
        """
			renvoie l'agrement d'accord en fonction du nombre qui est passé
			si >1 alors 'pl' pluriel
			sinon 'sg' singulier
		"""
        nb = 1.0
        try:
            nb = float(nombre)
        except:
            nb = 1.0
        if nb > 1:
            return 'pl'
        return 'sg'

    def lexicalizeParty(self, message, partyName):
        """
			renvoi le nom du parti / groupe parlementaire
		"""
        str = ""
        try:
            str = EPG[partyName]
        except:
            return partyName

# if (isInList(NP, partyName)):
            # 	return partyName
            # else:
            # 	return "ERROR"
        return str

    def lexicalizeNationality(self, message, country):
        """
			renvoi la nationalité à partir du pays
		"""
        str = ""
        try:
            str = CtN[country]
        except:
            return "ERROR"
        else:
            return str

    def lexicalizeYear(self, message, year):
        """
			vérifie que c'est bien une année qui est passée
		"""
        try:
            int(year)
        except:
            return "ERROR"
        else:
            return year

    def lexicalizeGender(self, message, gender):
        """
			renvoi le genre
		"""
        if (gender == 'Male'):
            return "men"
        elif (gender == 'Female'):
            return "women"
        else:
            print("gender error")
            return "ERROR"

    def lexicalizeAgreement(self, message, sim):
        """
			lexicalise le consensus (ou disensus) plus ou moins fort de 2 groupes d'individus
		"""
        if (sim < 0 or sim > 100):
            return "ERROR"
        elif (sim > 75):
            return "in agreement"
        elif (sim > 50):
            return "relatively in agreement"
        elif (sim > 25):
            return "relatively in disagreement"
        else:
            return "in disagreement"

    def lexicalizeAgreementChange(self, message, sim):
        """
			lexicalise le consensus (ou disensus) plus ou moins fort de 2 groupes d'individus
		"""
        if (sim < 0 or sim > 100):
            return "ERROR"
        elif (sim > 75):
            return "strong agreement"
        elif (sim > 50):
            return "agreement"
        elif (sim > 25):
            return "disagreement"
        else:
            return "strong disagreement"

    def lexicalizeSimilarityDifference(self, message, sim):
        """
			lexicalise la modification de similarité
		"""
        if (sim < 0):
            return "an increase"
        else:
            return "a decrease"

    def lexicalizeDate(self, message, date):
        """
			lexicalise une date
		"""
        return date

    def lexicalizeTense(self, message, tense):
        """
			lexicalise le temps du verbe
		"""
        if (tense == "present"):
            return "present"
        elif (tense == "past"):
            return "past"
        else:
            print("tense error")
            return "ERROR"


def is_template_filled(template):
    """
		Fonction qui verifie si une instance de patron a été completement instanciée
	"""
    if isinstance(template, dict):
        for key in template:
            if not is_template_filled(template[key]):
                return False
    elif isinstance(template, str):
        return not template.startswith("lexicalize")
    return True


def applyTemplate(template, message):
    """
		check if a template can be applied to an input message
		if template is a dictionnary, apply recursively to all keys
		return (boolean, instance) where the boolean indicate whether or not the template has been successfully instantiated.
	"""
    # the lexicalizer
    lex = Lexicalize()
    # if a dict apply to all keys
    if isinstance(template, dict):
        for key in template:
            (filled, val) = applyTemplate(template.get(key), message)
            if filled:
                template[key] = val
            else:
                return (False, template)

        return (True, template)
    #if a scalar then apply directly
    else:
        # if there is call to lexicalisation then treat it
        if template.startswith("lexicalize"):
            val = lex.lexicalize(message, template)
            if ("ERROR" in val):
                return (False, val)
            return (True, val)
        # if the value of template is a key of message subsitute the value of template by the value of message
        elif template in message:
            return (True, message[template])
        #otherwise not found template unchanged
        return (True, template)


def applyTemplates(temps, message):
    """
		Choisi la collection de patron à appliquer au message en fonction de la classe du message
		si un patron peut être appliqué alors on retourne une instance du patron dans laquelle tous les elements à lexicaliser l'ont été.
		Sinon, retourne None
	"""
    best_temp = None
    #choix de la classe de patron adequate
    if message['class'] in temps:
        templates = temps[message['class']]
    else:
        return None

    # application de tous les patrons de la classe
    for t in templates:
        temp = t.copy(deep=True)
        (matches, temp) = applyTemplate(temp, message)
        if matches:
            best_temp = temp
    return best_temp


def toProtoPhrase(templates, docPlan, data_path):
    """
		fonction effectuant la micro-plannification des messages du plan de document
		docPlan : la collection de messages organisés en paragraphe
		templates : l'ensemble des patrons qui servieront à la plannification
	"""
    protoPhrases = []
    # pour chaque paragraphe
    global EPG, CtN
    EPG = getEPG(data_path + '/EP_GROUPS.csv')
    CtN = getCtN(data_path + '/Country_Nationality.csv')

    for p in docPlan:
        para = []
        for m in p:
            # pour chaque message on essaye d'appliquer les patrons
            proto = applyTemplates(templates, m['message'])
            if proto != None:
                para.append(proto)
        protoPhrases.append(para)
    return protoPhrases
