import csv

"""
    Sert à la lexicalisation des partis nationnaux (en vérifiant si le nom du parti existe réellement dans la base de données.
"""

def readCSVfile(filename):
    """
        ouverture d'un fichier csv en utilisant le délimiter de champs ';'
    """
    csvfile = open(filename, newline='', encoding="utf-8-sig")
    reader = csv.DictReader(csvfile, delimiter='\t')
    return reader

def isInList(l, e):
    for elt in l:
        if (elt == e):
            return True
    return False

def insertInList(l, e):
    if (isInList(l,e)):
        return l
    else:
        l += [e]
        return l

def getNP(filename):
    reader = readCSVfile(filename)
    national_party = []

    for e in reader:
        insertInList(national_party, e['NATIONAL_PARTY'])


    return national_party
