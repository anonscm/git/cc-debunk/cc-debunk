# -*- coding: utf-8 -*-

from nltk import FeatStruct



def toMotifPlan(result, booltab, booltab1, booltab2):
    """
        réalise le plan d'un texte décrivant un motif
        chaque message et étiquetté par une classe qui permet de choisir le type de template à utiliser
    """

    #'g1':groupAnalysis(e['g1']), 'g2':groupAnalysis(e['g2']), 'context_size':int(e['|subgroup(context)|']), 'g1_size':int(e['|subgroup(g1)|']), 'g2_size':int(e['|subgroup(g2)|']), 'reviews_size':int(e['|ratings(c,g1,g2)|']), ), 'quality':float(e['quality'])}
    motifPlan = []

    context = result['subject']
    date = result['date']
    ref_sim = result['ref_sim']
    pattern_sim = result['pattern_sim']
    g1_size = result['g1_size']
    g2_size = result['g2_size']
    ballots_number = result['context_size']


    #gestion du TEMPS
    tense = ""
    if (booltab[1]):
        tense = "past"
    else:
        tense = "present"

    paragraphe = []

    #gestion des attributs à envoyer pour les groupes d'individus
    #groupe 1
    tmp = ""
    if (booltab1[0] and not booltab1[1]):
        tmp += "EPG='" + result['g1_EP_Group'] + "',"
    if (booltab1[1]):
        tmp += "NP='" + result['g1_NatParty'] + "',"
    if (booltab1[2]):
        tmp += "Country='" + result['g1_Country'] + "',"
    if (booltab1[4]):
        tmp += "Gender='" + result['g1_Gender'] + "',"
    if (booltab1[3]):
        if (booltab1[6]):
            tmp+= "date1='" + result['g1_Age'][0] + "',date2='" + result['g1_Age'][1].strip(' ') + "',"
        else:
            tmp+= "date='" + result['g1_Age'][0] + "',"
    tmp += "tense=" + tense
    group1_description = FeatStruct("[message=[class='group_description1',group_number='first'," + tmp + "]]")
    #groupe 2
    tmp = ""
    if (booltab2[0] and not booltab2[1]):
        tmp += "EPG='" + result['g2_EP_Group'] + "',"
    if (booltab2[1]):
        tmp += "NP='" + result['g2_NatParty'] + "',"
    if (booltab2[2]):
        tmp += "Country='" + result['g2_Country'] + "',"
    if (booltab2[4]):
        tmp += "Gender='" + result['g2_Gender'] + "',"
    if (booltab2[3]):
        if (booltab2[6]):
            tmp+= "date1='" + result['g1_Age'][0] + "',date2='" + result['g1_Age'][1].strip(' ') + "',"
        else:
            tmp+= "date='" + result['g1_Age'][0] + "',"
    tmp += "tense=" + tense
    group2_description = FeatStruct("[message=[class='group_description2',group_number='second'," + tmp + "]]")
    paragraphe.append(group1_description)
    paragraphe.append(group2_description)

    tmp = "[message=[class='global_similarity',sim_tot=" + str(ref_sim) + ",tense=" + tense + "]]"
    global_similarity = FeatStruct(tmp)
    paragraphe.append(global_similarity)

    motifPlan.append(paragraphe)

    paragraphe = []

    tmp = "[message=[class='change',sim_rel=" + str(pattern_sim) + ",ballots_number=" + str(ballots_number) + ",tense=" + tense
    if (booltab[1]):
        if(booltab[2]):
            tmp += ",date1=\"" + str(date[0]) + "\",date2=\"" + str(date[1]) + "\""
        else:
            tmp += ",date=\"" + str(date[0]) + "\""
    tmp += "]]"
    change = FeatStruct(tmp)
    paragraphe.append(change)

    tmp = "[message=[class='change_values',sim_diff=" + str(ref_sim - pattern_sim) + ",sim_tot=" + str(ref_sim) + ",sim_rel=" + str(pattern_sim) + ",tense=" + tense + "]]"
    change_values = FeatStruct(tmp)
    paragraphe.append(change_values)

    motifPlan.append(paragraphe)

    paragraphe = []

    if (booltab[0]):
        subjects = FeatStruct("[message=[class = motif_subjects_intro,change =" + str(int((ref_sim-pattern_sim))) + ",tense=" + tense + ",nb_subject=" + str(len(context)) + "]]")
        paragraphe.append(subjects)
        for elt in context:
            subject = FeatStruct("[message=[class = motif_subject,description =\"" + str(elt) + "\"]]")
            paragraphe.append(subject)
    complement = FeatStruct("[message=[class = statistics,g1_size=" + str(g1_size) + ",g2_size=" + str(g2_size)+ ",tense=" + tense + "]]")
    paragraphe.append(complement)

    motifPlan.append(paragraphe)

    return motifPlan
