# -*- coding: utf-8 -*-

import csv

"""
    Sert à lexicaliser les nationnalités des députés, en lisant le ficheir csv contenant le pays, et la nationnalité.
"""

def readCSVFile(filename):
    """
        ouverture d'un fichier csv en utilisant le délimiter de champs ';'
    """
    csvfile = open(filename, newline='', encoding="utf-8")
    reader = csv.DictReader(csvfile, delimiter='\t')
    return reader

def getCtN(filename):
    reader = readCSVFile(filename)
    CtN = {}

    for e in reader:
        CtN[e['Country']] = e['Nationality']

    return CtN
