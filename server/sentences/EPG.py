# -*- coding: utf-8 -*-

import csv

"""
    Sert à lexicaliser les groupes du parlement européeen, en lisant le ficheir csv contenant le nom complet du groupe et son abréviation.
"""

def readCSVFile(filename):
    """
        ouverture d'un fichier csv en utilisant le délimiter de champs ';'
    """
    csvfile = open(filename, newline='', encoding="utf-8")
    reader = csv.DictReader(csvfile, delimiter=';')
    return reader

def getEPG(filename):
    reader = readCSVFile(filename)
    EPG = {}

    for e in reader:
        EPG[e['ShortName']] = e['Full Name']


    return EPG
