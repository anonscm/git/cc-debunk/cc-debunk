package realisation;

import simplenlg.features.*;
import simplenlg.framework.NLGFactory;
import simplenlg.phrasespec.*;
import simplenlg.framework.PhraseElement;

/*
 * Implémente une structure de groupe verbal permettant d'ajouter des attributs et construisant les structures simpleNLG
 */
public class VP extends AbstractPhraseStruct {
	Tense tense;
	Boolean isTense = true;
	Boolean isPassive = false;
	
	private void setTense(String tense) {
		if (tense.equals("present")) {
			this.tense = Tense.PRESENT;
		} else if (tense.equals("past")) {
			this.tense = Tense.PAST;
		} else if (tense.equals("")) {
			this.isTense = false;
		} else {
			System.out.println("[ERROR] LE TEMPS N'EST PAS CONNU");
		}
	}
	
	public PhraseElement generate() {
		VPPhraseSpec verb = factory.createVerbPhrase(this.lex);
		if (isTense) {
			verb.setFeature(Feature.TENSE, this.tense);
		}
		if (isPassive) {
			verb.setFeature(Feature.PASSIVE, true);
		}
		return verb;
	}
	
	public VP(String lex, String tense, Boolean passive, NLGFactory factory) {
		super(lex, factory);
		this.setTense(tense);
		isPassive = passive;
		isTense = true;
	}
	
	public VP(String lex, NLGFactory factory) {
		super(lex, factory);
	}
}
