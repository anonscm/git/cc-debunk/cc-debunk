package realisation;

import simplenlg.features.Feature;
import simplenlg.framework.NLGFactory;
import simplenlg.phrasespec.*;
import simplenlg.framework.PhraseElement;

/*
 * Implémente une structure de phrase permettant d'ajouter les éléments nécéssaires et construisant les structures simpleNLG
 */
public class Sentence extends AbstractPhraseStruct {
	VP verb;
	NP subject;
	NP object;
	boolean isObject;
	
	public PhraseElement generate() {
		SPhraseSpec phrase = this.factory.createClause();
		VPPhraseSpec verb = (VPPhraseSpec) this.verb.generate();
		phrase.setVerb(verb);
		phrase.setSubject(this.subject.generate());
		if (isObject) {
			phrase.setObject(this.object.generate());
		}
		phrase.setFeature(Feature.TENSE, verb.getTense());
		return phrase;
	}
	
	public Sentence(String lex, VP verb, NP subject, NP object, NLGFactory factory) {
		super(lex, factory);
		this.verb = verb;
		this.subject = subject;
		this.object = object;
		isObject = true;
	}
	
	public Sentence(String lex, VP verb, NP subject, NLGFactory factory) {
		super(lex, factory);
		this.verb = verb;
		this.subject = subject;
		isObject = false;
	}
}
