package realisation;

import java.io.*;
import java.util.Vector;

/*
 * Lit les fichiers d'entrée contenant les résultat de la plannification
 */
public class Reader {
	
	public Vector<Vector<String>> read(String[] filenames, String path) {
		Vector<Vector<String>> files = new Vector<Vector<String>>();
		Vector<String> lines = new Vector<String>();
		String line = null;
		
		for (String filename : filenames) {
			try {
				FileReader fileReader = new FileReader(path + "/" + filename);
				BufferedReader reader = new BufferedReader(fileReader);
				lines.add(filename);
				while((line = reader.readLine()) != null) {
					lines.add(line);
				}
				reader.close();
			}
	        catch(FileNotFoundException ex) {
	            System.out.println("Unable to open file '" + filename + "'");                
	        }
	        catch(IOException ex) {
	            System.out.println("Error reading file '" + filename + "'");
	        }
			files.add(new Vector<String>(lines));
			lines.clear();
			
		}
		return files;	
	}
	
	public Reader() {
		
	}
}
