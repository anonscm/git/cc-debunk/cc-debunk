package realisation;

import java.io.*;
import java.util.Vector;

import simplenlg.framework.*;
import simplenlg.lexicon.Lexicon;
import simplenlg.realiser.english.Realiser;

/*
 * Programme principal de la réalisation de surface appelant les autres classes du package. IL :
 * - lit les fichiers
 * - construit les structures simpleNLG
 * - réalise la phrase
 * - écris les résultats
 */
public class RealiserMain {


    /**
     * @param args
     */
    public void mainRealiser(String path_input, String path_output) {
        Lexicon lexicon = Lexicon.getDefaultLexicon();
        NLGFactory factory = new NLGFactory(lexicon);
        Realiser realiser = new Realiser(lexicon);

        Reader reader = new Reader();
        Writer writer = new Writer();
        Deserialization parser = new Deserialization(factory, realiser);
        String[] filenames = new File(path_input).list();
        Vector<Vector<String>> files_input = reader.read(filenames, path_input);


        Vector<Vector<String>> files_output = new Vector<Vector<String>>();
        Vector<String> lines_output = new Vector<String>();;
        for (int i = 0; i < files_input.size(); i++) {
        	Vector<String> lines_input = files_input.get(i);
        	lines_output.add(lines_input.get(0));
        	for (int j = 1; j < lines_input.size(); j++) {
        		lines_output.add(parser.parseGeneral(lines_input.get(j)));
        	}
        	System.out.println("\n\n");
        	files_output.add(new Vector<String>(lines_output));
        	lines_output.clear();
        }

        writer.write(files_output, path_output);

    }
}
