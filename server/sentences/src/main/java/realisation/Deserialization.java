package realisation;

import java.util.Vector;

import simplenlg.features.Feature;
import simplenlg.framework.NLGFactory;
import simplenlg.framework.PhraseElement;
import simplenlg.phrasespec.PPPhraseSpec;
import simplenlg.realiser.english.Realiser;

public class Deserialization {
	NLGFactory factory;
	Realiser realiser;

	/*
	 * S'occuper de parser la structure générale d'une ligne
	 */
	public String parseGeneral(String line) {
		String[] substrings = line.split("\\(",2);
		switch (substrings[0]) {
			case "":
				return "";
			case "string":
				return this.parseString(substrings[1].substring(0, substrings[1].length()-1));
			case "sentence":
				return this.parseSentence(substrings[1].substring(0, substrings[1].length()-1));
			default :
				System.out.println("ERROR de début de phrase");
				return "";
		}
	}

	/*
	 * Parse un string
	 */
	public String parseString(String string) {
		return string.replace("\"","").split("=",2)[1];
	}

	/*
	 * Parse une structure de phrase puis réalise cette phrase
	 */
	public String parseSentence(String string) {
		Vector<String> headers = new Vector<String>();
		Vector<String> fields = new Vector<String>();
		this.parseParanthese(string, headers, fields);
		for (int j = 0; j < headers.size(); j++) {
			headers.set(j, headers.get(j).replace(",", ""));
		}


		Boolean isVerb = false;
		Boolean isObject = false;
		Boolean isFm = false;
		Boolean isComplement = false;
		Boolean isPonctuation = false;
		String complement = "";
		String fm = "";
		String ponctuation = "";

		VP verb = new VP("", this.factory);
		NP object = new NP("", this.factory);
		NP subject = new NP("", this.factory);


		for (int i = 0; i < headers.size(); i++) {
			if (headers.get(i).equals("fm")) {
				fm = parseString(fields.get(i).split("\\(", 2)[1].replace(")",""));
				isFm = true;
			} else if (headers.get(i).equals("vp")) {
				verb = this.parseVP(fields.get(i));
				isVerb = true;
			} else if (headers.get(i).equals("np")) {
				if (isVerb) {
					object = this.parseNP(fields.get(i));
					isObject = true;
				} else {
					subject = this.parseNP(fields.get(i));
				}
			} else if (headers.get(i).equals("string")) {
				complement = parseString(fields.get(i));
				isComplement = true;
			} else if (headers.get(i).equals("ponctuation")) {
				isPonctuation = true;
				ponctuation = parsePonctuation(fields.get(i));
			} else if (headers.get(i).equals("")) {
			} else {
				System.out.println("ERROR de phrase");
			}
		}

		Sentence sentence;
		if (isObject) {
			sentence = new Sentence("", verb, subject, object, this.factory);
		} else {
			sentence = new Sentence("", verb, subject, this.factory);
		}

		PhraseElement phrase = sentence.generate();
		if (isFm) {
			PPPhraseSpec frontModifier = this.factory.createPrepositionPhrase(fm);
			frontModifier.setFeature(Feature.APPOSITIVE, true);
			phrase.addFrontModifier(fm);
		}
		if (isComplement) {
			phrase.addComplement(complement);
		}

		String realisedSentence = realiser.realiseSentence(phrase);
		if (isPonctuation) {
			StringBuffer strBuf = new StringBuffer(realisedSentence);
			strBuf.setCharAt(realisedSentence.length()-1,':');
			realisedSentence = strBuf.toString();
		}
		return realisedSentence;
	}

	/*
	 * Parse un groupe nominal
	 */
	public NP parseNP(String string) {
		Vector<String> headers = new Vector<String>();
		Vector<String> fields = new Vector<String>();
		this.parseParanthese(string, headers, fields);

		Vector<String> modifiers = new Vector<String>();
		Vector<String> premodifiers = new Vector<String>();
		Vector<String> complements = new Vector<String>();
		String number = "";
		String lex = "";

		String[] params;
		String[] arg;
		for (int j = 0; j < headers.size(); j++) {
			params = headers.get(j).split(",");
			for (int i = 0; i < params.length; i++) {
				arg = params[i].split("=");
				if (arg[0].equals("lex")) {
					lex = arg[1];
				} else if (arg[0].equals("modifier")) {
					modifiers.add(arg[1]);
				} else if (arg[0].equals("premodifier")) {
					premodifiers.add(arg[1]);
				} else if (arg[0].equals("number")) {
					number = arg[1];
				} else if (arg[0].equals("complement")) {
					if (arg[1].equals("string")) {
						complements.add(this.parseString(fields.get(j)));
					}
				} else if (arg[0].equals("")) {
				}
				else {
					System.out.println("ERROR de groupe nominal");
				}
			}
		}
		NP noun = new NP(lex, modifiers, premodifiers, complements, number, this.factory);
		return noun;
	}

	/*
	 * Parse un groupe verbal
	 */
	public VP parseVP(String string) {
		String[] params = string.split(",");
		String[] arg;
		String lex = "";
		String tense = "";
		Boolean isPassive = false;
		for (String param : params) {
			arg = param.split("=");
			if (arg[0].equals("lex")) {
				lex = arg[1];
			} else if (arg[0].equals("tense")) {
				tense = arg[1];
			} else if (arg[0].equals("passive")) {
				isPassive = true;
			} else {
				System.out.println("ERROR de groupe verbal");
			}
		}
		VP verb;
		verb = new VP(lex, tense, isPassive, this.factory);
		return verb;
	}

	/*
	 * Récupère les données hors parenthèses d'un string, qui vont dans le tableau headers
	 * Et les données dans les parenthèses qui vont dans le tableau fields
	 * ex : la string "np(head='group',nb=pl),vp(head='have',tense=past)" va donner:
	 * headers = ["np";"vp"]
	 * fields = ["head='group',nb=pl";"head='have',tense=past"]
	 */
	public void parseParanthese(String line, Vector<String> headers, Vector<String> fields) {
		int i = 0;
		int countPar = 0;
		int tmp = 0;
		while (i < line.length()) {
			if (line.charAt(i) == '(') {
				if (countPar == 0) {
					headers.add(line.substring(tmp, i));
					tmp = i + 1;
				}
				countPar++;
			} else if (line.charAt(i) == ')') {
				countPar--;
				if (countPar == 0) {
					fields.add(line.substring(tmp, i));
					tmp = i + 1;
				}
			}
			i++;
		}
		headers.addElement(line.substring(tmp));
	}

	public String parsePonctuation(String str) {
		if (str.equals("colon")) {
			return ":";
		} else {
			System.out.println("ERROR ponctuation");
			return "";
		}
	}

	public Deserialization(NLGFactory factory, Realiser realiser) {
		this.factory = factory;
		this.realiser = realiser;
	}
}
