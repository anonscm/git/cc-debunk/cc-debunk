package realisation;

import java.io.*;
import java.util.Vector;

/*
 * Ecrit les phrases générées par le package dans les fichiers adéquat
 */
public class Writer {
	
	public Vector<Vector<String>> write(Vector<Vector<String>> files, String path) {
		String filename;
		for (Vector<String> lines : files) {
			filename = lines.get(0);
			lines.remove(0);
			try {
				filename = path + "/" + filename + ".txt";
				PrintWriter writer = new PrintWriter(filename, "utf-8");
				for (String line : lines) {
					writer.println(line);
				}	
				writer.close();
			}
	        catch(FileNotFoundException ex) {
	            System.out.println("Unable to open file '" + filename + "'");                
	        }
	        catch(IOException ex) {
	            System.out.println("Error writting file '" + filename + "'");
	        }			
		}
		return files;
	}
	
	public Writer() {
		
	}
}
