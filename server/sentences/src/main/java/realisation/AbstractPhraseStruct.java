package realisation;

import simplenlg.framework.NLGFactory;
import simplenlg.framework.PhraseElement;


/* 
 * Classe abstraite pour décrire les élements d'une phrase et leur associer leur structure issue de simpleNLG adéquat
 */

public abstract class AbstractPhraseStruct {
	String lex;
	NLGFactory factory;
	
	abstract PhraseElement generate();
	
	public AbstractPhraseStruct(String lex, NLGFactory factory) {
		this.lex = lex;
		this.factory = factory;
	}
}
