package realisation;

import java.util.Vector;

import simplenlg.features.*;
import simplenlg.framework.NLGFactory;
import simplenlg.framework.PhraseElement;
import simplenlg.phrasespec.NPPhraseSpec;

/*
 * Implémente une structure de groupe nominal permettant d'ajouter des attributs et construisant les structures simpleNLG
 */
public class NP extends AbstractPhraseStruct {
	Vector<String> modifiers;
	Vector<String> premodifiers;
	Vector<String> complements;
	NumberAgreement number;
	Boolean isNumber = true;
	
	public void setNumber(String number) {
		if (number.equals("sg")) {
			this.number = NumberAgreement.SINGULAR;
		} else if (number.equals("pl")) {
			this.number = NumberAgreement.PLURAL;
		} else if (number.equals("")){
			this.isNumber = false;
		} else {
			System.out.println("[ERROR] LE NOMBRE N'EST PAS CONNU");
		}
	}
	
	public PhraseElement generate() {
		String[] words = this.lex.split(" ");
		NPPhraseSpec noun = this.factory.createNounPhrase();
		switch (words.length) {
			case 1:
				noun.setNoun(words[0]);
				break;
			case 2:
				noun.setNoun(words[1]);
				noun.setDeterminer(words[0]);
				break;
			default:
				System.out.println("ERROR trop de mots dans le groupe nominal");
		}
		for (String modifier : modifiers) {
			noun.addModifier(modifier);
		}
		for (String premodifier : premodifiers) {
			noun.addPreModifier(premodifier);
		}
		for (String complement : complements) {
			noun.addModifier(complement);
		}
		if (isNumber) {
			noun.setFeature(Feature.NUMBER, number);
		}
		return noun;
	}
	
	public NP(String lex, Vector<String> modifiers, Vector<String> premodifiers, Vector<String> complements, String number, NLGFactory factory) {
		super(lex, factory);
		this.modifiers = modifiers;
		this.premodifiers = premodifiers;
		this.complements = complements;
		this.setNumber(number);
	}
	
	public NP(String lex, NLGFactory factory) {
		super(lex, factory);
	}
}
