package realisation;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {
        String pythonExecutable = args[0];
        String inputFile = args[1];
        String intermediaryPath = args[2];
        String outputPath = args[3];
        String dataPath = args[4];

        ProcessBuilder ps = new ProcessBuilder(pythonExecutable, "main.py", inputFile, intermediaryPath, dataPath);

        ps.redirectErrorStream(true);

        Process pr = ps.start();

        BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }

        try {
            pr.waitFor();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        in.close();

        //effectue la réalisation de surface
        RealiserMain realiser = new RealiserMain();
        realiser.mainRealiser(intermediaryPath, outputPath);
    }
}
