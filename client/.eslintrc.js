module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/recommended',
    'plugin:vue/essential',
    '@vue/standard',
    '@vue/typescript'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-eval': 'off',
    'camelcase': 'off',
    'vue/singleline-html-element-content-newline': [
      'warning',
      {
        ignores: [
          'span',
          'em',
          'a'
        ]
      }
    ],
    'vue/multiline-html-element-content-newline': [
      'warning',
      {
        ignores: [
          'span',
          'em',
          'a'
        ]
      }
    ],
    'vue/no-v-html': 'off',
    'vue/component-name-in-template-casing': 'warning'
  },
  parserOptions: {
    parser: '@typescript-eslint/parser'
  }
}
