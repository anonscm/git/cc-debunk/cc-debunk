module.exports = {
  plugins: {
    'postcss-import': {},
    'postcss-preset-env': {},
    'postcss-nested': {},
    tailwindcss: {
      experiments: {
        shadowLookup: true
      }
    },
    autoprefixer: {},
    cssnano: {}
  }
}
