const PurgecssPlugin = require('purgecss-webpack-plugin')
const glob = require('glob-all')
const path = require('path')

class TailwindExtractor {
  static extract (content) {
    return content.match(/[A-Za-z0-9-_:/]+/g) || []
  }
}

module.exports = {
  devServer: {
    proxy: {
      '^/ws': {
        target: 'ws://localhost:8080',
        ws: true
      },
      '^/api': {
        target: 'http://localhost:8080'
      },
      '^/(getdataset|getdatasetFirstLine|getdatasetTwoLines|datasets|example|examples)': {
        target: 'http://localhost:8080'
      }
    },
    overlay: {
      warnings: true,
      errors: true
    }
  },

  configureWebpack: {
    optimization: {
      splitChunks: {
        chunks: 'all',
        maxInitialRequests: Infinity,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name (module) {
              // get the name. E.g. node_modules/packageName/not/this/part.js
              // or node_modules/packageName
              const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]

              // npm package names are URL-safe, but some servers don't like @ symbols
              return `npm.${packageName.replace('@', '')}`
            }
          }
        }
      }
    },
    plugins: [
      new PurgecssPlugin({
        paths: glob.sync([
          path.join(__dirname, './public/index.html'),
          path.join(__dirname, './src/**/*.vue'),
          path.join(__dirname, './src/**/*.ts'),
          path.join(__dirname, './src/**/*.js')
        ]),
        extractors: [{
          extractor: TailwindExtractor,
          extensions: [ 'html', 'js', 'ts', 'vue' ]
        }],
        keyframes: true,
        whitelistPatterns: [
          /router-link(-exact)?-active/,
          // next three: Vue transitions
          /-active$/,
          /-enter$/,
          /-leave-to$/
        ],
        whitelistPatternsChildren: [
          // vue-good-table
          /vgt/,
          // vue-awesome
          /fa-/
        ]
      })
    ]
  }
}
