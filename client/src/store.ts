/* globals localStorage */

import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'
import * as _ from 'lodash/fp'
import * as util from './util'

import Dataset from '@/types/Dataset'

Vue.use(Vuex)

interface RootState {
  datasets: Dataset[]
  dataset: Dataset
  examples: any[]
  example: any
  contextData: any
  userData: any
  reviewData: any
  contextColumns: string[]
  userColumns: string[]
  reviewColumns: string[]
  settings: any
  userID: string
  jobID: string
  status: string
}

function localStorageOrNull (key: string) {
  return localStorage.getItem(key) || null
}

const state: RootState = {
  datasets: [],
  dataset: null,
  examples: [],
  example: null,
  contextData: [],
  userData: [],
  reviewData: [],
  contextColumns: [],
  userColumns: [],
  reviewColumns: [],
  settings: util.getSavedSettings(),
  userID: localStorageOrNull('userID'),
  jobID: localStorageOrNull('jobID'),
  status: ''
}

export default new Vuex.Store<RootState>({
  strict: process.env.NODE_ENV !== 'production',
  state,
  actions: {
    fetch_datasets ({
      state,
      commit
    }) {
      if (!_.isEmpty(state.datasets)) {
        return
      }

      util.apiGet('/datasets')
        .then(({ data: datasets }) => {
          commit('set_datasets', datasets)
          const savedDataset = _.get('dataset', state.settings)
          const dataset = (savedDataset
            ? _.find(
              _.matchesProperty('name', savedDataset.name),
              datasets)
            : _.head(datasets)) || _.head(datasets)

          commit('set_dataset', dataset)
        })
    },

    fetch_dataset ({
      commit,
      state
    }, dataset) {
      if (dataset === state.dataset && !_.isEmpty(state.userData)) {
        return Promise.resolve()
      }

      if (dataset !== state.dataset) {
        commit('unselect_all')
      }

      commit('set_dataset', dataset)

      return axios.all([
        axios.post('/getdataset', {
          file: 'objects_file',
          dataset
        }),
        axios.post('/getdataset', {
          file: 'individuals_file',
          dataset
        }),
        axios.post('/getdatasetTwoLines', {
          file: 'reviews_file',
          dataset
        })
      ]).then(([{
        data: contextData
      }, {
        data: userData
      }, {
        data: reviewData
      }]) => {
        commit('set_data', {
          contextData,
          userData,
          reviewData
        })
      })
    },

    fetch_examples ({
      commit
    }) {
      util.apiGet('/examples')
        .then(({ data: examples }) => {
          commit('set_examples', examples)
        })
    },

    fetch_example ({ commit }, example) {
      commit('set_example', example)

      return axios.get('/example', {
        params: {
          name: example.name
        }
      })
    },

    import_settings ({ commit, dispatch, state }, data) {
      return new Promise((resolve, reject) => {
        const dataset = _.defaultTo(_.head(state.datasets), _.find(_.matchesProperty('name', _.get('dataset.name', data)), state.datasets))
        dispatch('fetch_dataset', dataset).then(() => {
          commit('SET_SETTINGS', data)
          resolve()
        })
      })
    }
  },

  mutations: {
    setUserID (state, id) {
      localStorage.setItem('userID', id)
      state.userID = id
    },

    setJobID (state, id) {
      localStorage.setItem('jobID', id)
      state.jobID = id
    },

    set_dataset (state, dataset) {
      state.dataset = dataset
    },

    set_datasets (state, datasets) {
      state.datasets = datasets
    },

    set_examples (state, examples) {
      state.examples = examples
    },

    set_example (state, example) {
      state.example = example
    },

    unselect_all (state) {
      state.settings.individuals_1_scope = []
      state.settings.individuals_2_scope = []
      state.settings.individuals_scope = []
      state.settings.contexts_scope = []
      state.settings.description_attributes_individuals = []
      state.settings.description_attributes_objects = []
      util.saveSettings(state.settings)
    },

    SET_SETTINGS (state, settings) {
      state.settings = Object.assign({}, state.settings, settings)
      util.saveSettings(state.settings)
    },

    set_data (state, {
      userData,
      contextData,
      reviewData
    }) {
      const numberizeNested = _.map(_.map((x: any) => isNaN(x) ? x : parseFloat(x)))
      const objectifyCSV = (data: ArrayLike<ArrayLike<string | number | symbol>>) => _.map(_.zipObject(_.head(data) || []), _.tail(data))
      const format = _.compose(objectifyCSV, numberizeNested)
      state.userData = format(userData)
      state.userColumns = _.head(userData)
      state.contextData = format(contextData)
      state.contextColumns = _.head(contextData)
      state.reviewData = format(reviewData)
      state.reviewColumns = _.head(reviewData)

      state.settings.numericHeader = state.dataset.numericHeader
      state.settings.arrayHeader = state.dataset.arrayHeader
    },

    setStatus (state, status) {
      state.status = _.defaults({
        loading: false
      }, status)
    }
  }
})
