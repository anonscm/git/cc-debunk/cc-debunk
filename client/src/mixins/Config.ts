import { Vue, Component, Model, Watch } from 'vue-property-decorator'
import { State } from 'vuex-class'

import * as _ from 'lodash/fp'

const zipIndexes = (xs: any[]): Array<[any, number]> => _.zip(xs, _.range(0, xs.length))

@Component
export default class ConfigMixin extends Vue {
  @State dataset: any
  @State datasets: any
  @State contextData: any
  @State userData: any
  @State reviewData: any
  @State contextColumns: any
  @State userColumns: any
  @State reviewColumns: any
  @State userID: any

  json: any

  qual_functions = []
  qual_function : any = null

  @Model('config', { type: Object }) readonly config: object

  @Watch('json')
  onJSONChanged () {
    this.$emit('config', this.json)
  }

  get settings () {
    return this.$store.state.settings
  }

  set settings (settings: any) {
    this.$store.commit('SET_SETTINGS', settings)
  }

  get threshold_quality () {
    return this.settings.threshold_quality
  }

  set threshold_quality (threshold_quality: number) {
    this.$store.commit('SET_SETTINGS', { threshold_quality })
  }

  get threshold_objects () {
    return this.settings.threshold_objects
  }

  set threshold_objects (threshold_objects: number) {
    this.$store.commit('SET_SETTINGS', { threshold_objects })
  }

  get threshold_individuals () {
    return this.settings.threshold_individuals
  }

  set threshold_individuals (threshold_individuals: number) {
    this.$store.commit('SET_SETTINGS', { threshold_individuals })
  }

  get contexts_scope () {
    return this.settings.contexts_scope || []
  }

  set contexts_scope (contexts_scope: number) {
    this.$store.commit('SET_SETTINGS', { contexts_scope })
  }

  getHeaderType (o: string) {
    if (this.settings.arrayHeader.includes(o)) {
      return 'themes'
    } else if (this.settings.numericHeader.includes(o)) {
      return 'numeric'
    } else {
      return 'simple'
    }
  }

  get exportSettings () {
    return `data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(this.json))}`
  }

  get contextSelectedColumns () {
    return _.map(([obj, _type]) => obj)(this.settings.description_attributes_objects)
  }

  set contextSelectedColumns (columns) {
    const description_attributes_objects = _.map(
      (obj) => [obj, this.getHeaderType(obj)],
      columns)
    this.$store.commit('SET_SETTINGS', { description_attributes_objects })
  }

  get description_attributes_objects () {
    return this.$store.state.settings.description_attributes_objects
  }

  get userSelectedColumns () {
    return _.map(([obj, _type]) => obj)(this.settings.description_attributes_individuals)
  }

  set userSelectedColumns (columns) {
    const description_attributes_individuals = _.map(
      (obj) => [obj, this.getHeaderType(obj)],
      columns)
    this.$store.commit('SET_SETTINGS', { description_attributes_individuals })
  }

  get description_attributes_individuals () {
    return this.$store.state.settings.description_attributes_individuals
  }

  get contextTableColumns () {
    return _.map((name: string, index: number) => ({
      field: name,
      hidden: index === 0,
      label: name
    }), this.contextColumns)
  }

  get userTableColumns () {
    return zipIndexes(this.userColumns).map(([name, index]) => ({
      field: name,
      hidden: index === 0,
      label: name
    }))
  }

  importSettings (data: any) {
    this.$store.dispatch('import_settings', data).then(this.loadSettings)
  }

  mounted () {
    if (this.dataset) {
      this.$store.dispatch('fetch_dataset', this.dataset).then(this.loadSettings)
    }
  }

  loadSettings () {
    this.qual_function = _.find(
      _.matchesProperty(
        'value',
        this.settings.quality_measure
      ), this.qual_functions)
  }
}
