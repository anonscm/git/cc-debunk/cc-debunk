/* globals localStorage */

import axios from 'axios'
import * as _ from 'lodash/fp'

const defaultSettings = {
  threshold_objects: 10,
  threshold_individuals: 4,
  threshold_quality: 0.6,
  aggregation_measure: 'VECTOR_VALUES',
  similarity_measure: 'MAAD',
  quality_measure: 'DISAGR_SUMDIFF',
  algorithm: 'DSC+CLOSED+UB2',
  timebudget: 3600
}

export function getSavedSettings () {
  function saveAndReturnDefault () {
    saveSettings(defaultSettings)
    return defaultSettings
  }

  try {
    const settings = localStorage.getItem('settings')
    if (_.isEmpty(settings)) {
      return saveAndReturnDefault()
    }
    return JSON.parse(settings || '')
  } catch (e) {
    return saveAndReturnDefault()
  }
}

export function saveSettings (settings: any) {
  localStorage.setItem('settings', JSON.stringify(settings))
}

export const apiGet = (q: string) => axios.get(q)
