export default interface Dataset {
  name: string
  longName: string
  type: string
  numericHeader: string[]
  arrayHeader: string[]
  selectableContextColumns: string[]
  selectableUserColumns: string[]
  url?: string
  description?: string
}
