import Vue from 'vue'
import App from './App.vue'
import store from './store'

import VueRouter from 'vue-router'

import Home from './views/Home.vue'
import Config from './views/Config.vue'
import Results from './views/Results.vue'
import Legal from './views/Legal.vue'
import About from './views/About.vue'

import _ from 'lodash/fp'

import 'drag-drop-touch'

Vue.use(VueRouter)

Vue.config.productionTip = false

const isDev = process.env.NODE_ENV !== 'production'
Vue.config.devtools = isDev
Vue.config.performance = isDev

const routes = [{
  path: '/',
  component: Home
}, {
  path: '/configuration',
  component: Config,
  beforeEnter (_to: any, _from: any, next: Function) {
    if (!_.isEmpty(store.state.userData) && !_.isEmpty(store.state.settings)) {
      next()
    } else {
      next('/')
    }
  }
}, {
  path: '/results',
  component: Results
}, {
  path: '/legal',
  component: Legal
}, {
  path: '/about',
  component: About
}, {
  path: '*',
  redirect: '/'
}]

const router = new VueRouter({
  routes,
  mode: 'history'
})

Vue.filter('percentage', (val: number) => `${_.round(_.multiply(100, val))}%`)

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
