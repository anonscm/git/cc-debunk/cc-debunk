# Server
## Docker
The easiest way to run the server is to use Docker, with `docker-compose`.

You will need a running `docker` daemon. The `docker-compose.yml` file is
located in `server`; you can launch and build the server with the following
command:

```
docker-compose up
```

## Configuration

You have to create a JSON configuration file in the `../ancore_config` folder; you can copy
`server.config.example.json`.

# Client

To install dependencies:
```
npm install
```

Then, to serve the application with a development server:
```
npm run serve
```

Or, to build everything:
```
npm run build
```

You can then deploy the `dist` folder.
