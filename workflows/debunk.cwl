#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool

inputs:
  dataset:
    type: Directory
  debunk:
    type: Directory
  settings:
    doc: Settings file
    type: File
    streamable: true

outputs:
  results:
    type: File
    format: text/csv
    outputBinding:
      glob: results-*[0-9].csv
  detailedResults:
    type: Directory
    outputBinding:
      glob: detailed_results-*[0-9]/

baseCommand: sh

arguments:
  - valueFrom: |
      pypy -c "
      import json, sys, time
      now = str(int(time.time() * 1000))
      with open('$(inputs.settings.path)') as f:
          data = json.load(f)
      data.update({
          'objects_file': '$(inputs.dataset.path)' + '/items.csv',
          'individuals_file': '$(inputs.dataset.path)' + '/users.csv',
          'reviews_file': '$(inputs.dataset.path)' + '/reviews.csv',
          'results_destination': 'results-' + now + '.csv',
          'detailed_results_destination': 'detailed_results-' + now + '/'
      })
      with open('settings.json', 'w') as f:
          json.dump(data, f)
      "
      pypy $(inputs.debunk.path)/main.py settings.json -q
    prefix: -c

hints:
  DockerRequirement:
    dockerImageId: debunk
    dockerFile: |
      FROM pypy:2 as builder
      WORKDIR /wheels
      RUN pip wheel cachetools intbitset numpy sortedcollections

      FROM pypy:2-slim
      COPY --from=builder /wheels /wheels
      RUN pip install cachetools intbitset numpy sortedcollections -f /wheels \
        && rm -rf /wheels \
        && rm -rf /root/.cache/pip/*
