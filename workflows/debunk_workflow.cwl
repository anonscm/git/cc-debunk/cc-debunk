#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SchemaDefRequirement:
    types:
      - $import: debunk.yaml

inputs:
  dataset: Directory
  debunk: Directory
  config: debunk.yaml#config

outputs:
  results:
    type: Any[]
    outputSource: convert-results/data-array
  # detailedResults:
  #   type: Directory
  #   outputSource: run-debunk/detailedResults

steps:
  make-config-file:
    run: gen_config_file.cwl
    in:
      config: config
    out:
      - settings

  run-debunk:
    run: debunk.cwl
    in:
      settings: make-config-file/settings
      debunk: debunk
      dataset: dataset
    out:
      - detailedResults
      - results

  convert-results:
    run: read_csv.cwl
    in:
      csv-file: run-debunk/results
    out:
      - data-array
