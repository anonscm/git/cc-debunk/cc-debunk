#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool

requirements:
  InlineJavascriptRequirement: {}

inputs:
  results-patterns: Any[]
  required-patterns: Any[]

outputs:
  missing-patterns:
    type: Any[]
    outputBinding:
      glob: missing.json
      loadContents: true
      outputEval: $(JSON.parse(self[0].contents))

baseCommand: node

arguments:
  - prefix: -e
    valueFrom: |
      const fs = require("fs")

      function convert (objs) {
        objs.forEach(o => delete o.ind)
        return objs.map(JSON.stringify)
      }

      const results = convert($(inputs["results-patterns"]))
      const required = convert($(inputs["required-patterns"]))

      const missing = required.filter(o => !results.includes(o)).map(JSON.parse)

      function write(name, data) {
        fs.writeFile(name + ".json", JSON.stringify(data), () => {})
      }

      write("missing", missing)

hints:
  DockerRequirement:
    dockerPull: node:alpine
