#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool

requirements:
  InlineJavascriptRequirement: {}

inputs:
  results1: Any[]
  results2: Any[]

outputs:
  only-in-1:
    type: Any[]
    outputBinding:
      glob: only_in_1.json
      loadContents: true
      outputEval: $(JSON.parse(self[0].contents))
  only-in-2:
    type: Any[]
    outputBinding:
      glob: only_in_2.json
      loadContents: true
      outputEval: $(JSON.parse(self[0].contents))
  identical:
    type: boolean
    outputBinding:
      glob: identical.json
      loadContents: true
      outputEval: $(JSON.parse(self[0].contents))

baseCommand: node

arguments:
  - prefix: -e
    valueFrom: |
      const fs = require("fs")

      const str_results1 = $(inputs.results1).map(JSON.stringify)
      const str_results2 = $(inputs.results2).map(JSON.stringify)

      const only_in_1 = str_results1.filter(o => !str_results2.includes(o)).map(JSON.parse)
      const only_in_2 = str_results2.filter(o => !str_results1.includes(o)).map(JSON.parse)

      const identical = only_in_1.length == 0 && only_in_2.length == 0

      function write(name, data) {
        fs.writeFile(name + ".json", JSON.stringify(data), () => {})
      }

      write("identical", identical)
      write("only_in_1", only_in_1)
      write("only_in_2", only_in_2)

hints:
  DockerRequirement:
    dockerPull: node:alpine
