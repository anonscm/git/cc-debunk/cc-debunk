#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool

requirements:
  InlineJavascriptRequirement: {}
  SchemaDefRequirement:
    types:
      - $import: debunk.yaml

inputs:
  config: debunk.yaml#config

outputs:
  settings:
    type: File
    outputBinding:
      glob: settings.json

hints:
  DockerRequirement:
    dockerPull: python:3-alpine

baseCommand: python
arguments:
  - prefix: -c
    valueFrom: |
      import json

      output = {
        "delimiter": "\\t",
        "nb_objects": 50000,
        "nb_individuals": 50000,
        "arrayHeader": $(inputs.config.arrayHeader),
        "numericHeader": $(inputs.config.numericHeader),
        "vector_of_outcome": None,
        "ponderation_attribute": None,
        "description_attributes_objects": $(inputs.config.description_attributes_objects),
        "description_attributes_individuals": $(inputs.config.description_attributes_individuals),
        "threshold_objects": $(inputs.config.threshold_objects),
        "threshold_individuals": $(inputs.config.threshold_individuals),
        "threshold_quality": $(inputs.config.threshold_quality),
        "aggregation_measure": "VECTOR_VALUES",
        "similarity_measure": "MAAD",
        "quality_measure": "$(inputs.config.quality_measure)",
        "algorithm": "DSC+CLOSED+UB2",
        "timebudget": 3600,
        "objects_scope": [],
        "individuals_1_scope": $(inputs.config.individuals_1_scope),
        "individuals_2_scope": $(inputs.config.individuals_2_scope)
      }
      with open("settings.json", "w") as f:
        json.dump(output, f)
      
