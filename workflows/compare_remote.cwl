#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SchemaDefRequirement:
    types:
      - $import: debunk.yaml
  SubworkflowFeatureRequirement: {}

inputs:
  config: debunk.yaml#config
  dataset: string
  expected-results: Any[]

outputs:
  identical:
    type: boolean
    outputSource: compare/identical
  only-in-results:
    type: Any[]
    outputSource: compare/only-in-1
  only-in-expected:
    type: Any[]
    outputSource: compare/only-in-2


steps:
  run-debunk:
    run: debunk_workflow_remote.cwl
    in:
      config: config
      dataset: dataset
    out:
      - results

  compare:
    run: compare.cwl
    in:
      results1: run-debunk/results
      results2: expected-results
    out:
      - identical
      - only-in-1
      - only-in-2
