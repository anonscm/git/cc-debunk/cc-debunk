#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SchemaDefRequirement:
    types:
      - $import: debunk.yaml

inputs:
  dataset: string
  config: debunk.yaml#config

outputs:
  results:
    type: Any[]
    outputSource: run-debunk/results

steps:
  make-config-file:
    run: gen_config_file_for_remote.cwl
    in:
      config: config
      dataset: dataset
    out:
      - settings

  run-debunk:
    run: debunk_remote.cwl
    in:
      settings: make-config-file/settings
    out:
      - results
