#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool

requirements:
  InlineJavascriptRequirement: {}

inputs:
  settings:
    doc: Settings file
    type: File
    streamable: true

outputs:
  results:
    type: Any
    outputBinding:
      glob: results.json
      loadContents: true
      outputEval: $(JSON.parse(self[0].contents))

baseCommand: node

arguments:
  - valueFrom: |
      const WebSocket = require("ws")
      const axios = require("axios")
      const fs = require("fs")

      const url = "https://contentcheck.liris.cnrs.fr"

      fs.readFile("$(inputs.settings.path)", { encoding: "utf8" }, (err, settings) => {
        const socket = new WebSocket("wss://contentcheck.liris.cnrs.fr/ws")

        socket.on("open", () => {
          socket.send(JSON.stringify({ data: settings }))
        })

        const onDone = ({ userID, jobID }) => {
          axios.get(url + "/api/results", { params: { userID, jobID }})
            .then(({ data }) => {
              fs.writeFile("results.json", JSON.stringify(data.results), () => {
                socket.close()
              })
            })
        }

        const onFailed = err => {
          socket.close()
          throw err
        }

        socket.on("error", error => {
          socket.close()
          throw error
        })

        socket.on("message", msg => {
          console.log('message !')
          console.log(msg)
          const data = JSON.parse(msg)

          switch (data.type) {
            case "done": {
              onDone(data.data)
              break
            }
            case "failed": {
              onFailed(data.data)
              break
            }
          }
        })
      })
    prefix: -e

hints:
  DockerRequirement:
    dockerImageId: debunk_remote
    dockerFile: |
      FROM node:alpine
      RUN yarn add ws axios
