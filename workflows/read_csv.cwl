#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool

requirements:
  InlineJavascriptRequirement: {}

inputs:
  csv-file:
    type: File
    streamable: true

outputs:
  data-array:
    type: Any[]
    outputBinding:
      glob: results.json
      loadContents: true
      outputEval: $(JSON.parse(self[0].contents))

baseCommand: node

arguments:
  - valueFrom: |
      const fs = require("fs")
      fs.readFile("$(inputs["csv-file"].path)", { encoding: "utf8" }, (err, data) => {
        const dataArray = []
        const lines = data.split("\\n")
        const headers = lines[0].split("\\t").map(s => s.replace(/\\|/, ""))
        for (let i = 1; i < lines.length; i++) {
          const row = {}
          lines[i].split("\\t").forEach((value, index) => {
            row[headers[index]] = value
          })
          dataArray.push(row)
        }
        fs.writeFile("results.json", JSON.stringify(dataArray), () => {})
      })
    prefix: -e

hints:
  DockerRequirement:
    dockerPull: node:alpine
