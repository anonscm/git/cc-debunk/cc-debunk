{
    "$graph": [
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "Any"
                    },
                    "id": "#compare.cwl/results1"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "Any"
                    },
                    "id": "#compare.cwl/results2"
                }
            ],
            "outputs": [
                {
                    "type": "boolean",
                    "outputBinding": {
                        "glob": "identical.json",
                        "loadContents": true,
                        "outputEval": "$(JSON.parse(self[0].contents))"
                    },
                    "id": "#compare.cwl/identical"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "Any"
                    },
                    "outputBinding": {
                        "glob": "only_in_1.json",
                        "loadContents": true,
                        "outputEval": "$(JSON.parse(self[0].contents))"
                    },
                    "id": "#compare.cwl/only-in-1"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "Any"
                    },
                    "outputBinding": {
                        "glob": "only_in_2.json",
                        "loadContents": true,
                        "outputEval": "$(JSON.parse(self[0].contents))"
                    },
                    "id": "#compare.cwl/only-in-2"
                }
            ],
            "baseCommand": "node",
            "arguments": [
                {
                    "prefix": "-e",
                    "valueFrom": "const fs = require(\"fs\")\n\nconst str_results1 = $(inputs.results1).map(JSON.stringify)\nconst str_results2 = $(inputs.results2).map(JSON.stringify)\n\nconst only_in_1 = str_results1.filter(o => !str_results2.includes(o)).map(JSON.parse)\nconst only_in_2 = str_results2.filter(o => !str_results1.includes(o)).map(JSON.parse)\n\nconst identical = only_in_1.length == 0 && only_in_2.length == 0\n\nfunction write(name, data) {\n  fs.writeFile(name + \".json\", JSON.stringify(data), () => {})\n}\n\nwrite(\"identical\", identical)\nwrite(\"only_in_1\", only_in_1)\nwrite(\"only_in_2\", only_in_2)\n"
                }
            ],
            "hints": [
                {
                    "dockerPull": "node:alpine",
                    "class": "DockerRequirement"
                }
            ],
            "id": "#compare.cwl"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "types": [
                        {
                            "type": "enum",
                            "name": "#debunk.yaml/quality_measure",
                            "symbols": [
                                "#debunk.yaml/quality_measure/AGR_SUMDIFF",
                                "#debunk.yaml/quality_measure/DISAGR_SUMDIFF"
                            ]
                        },
                        {
                            "name": "#debunk.yaml/config",
                            "type": "record",
                            "fields": [
                                {
                                    "type": {
                                        "type": "array",
                                        "items": "string"
                                    },
                                    "name": "#debunk.yaml/config/arrayHeader"
                                },
                                {
                                    "type": {
                                        "type": "array",
                                        "items": {
                                            "type": "array",
                                            "items": "string"
                                        }
                                    },
                                    "name": "#debunk.yaml/config/description_attributes_individuals"
                                },
                                {
                                    "type": {
                                        "type": "array",
                                        "items": {
                                            "type": "array",
                                            "items": "string"
                                        }
                                    },
                                    "name": "#debunk.yaml/config/description_attributes_objects"
                                },
                                {
                                    "type": {
                                        "type": "array",
                                        "items": "Any"
                                    },
                                    "name": "#debunk.yaml/config/individuals_1_scope"
                                },
                                {
                                    "type": {
                                        "type": "array",
                                        "items": "Any"
                                    },
                                    "name": "#debunk.yaml/config/individuals_2_scope"
                                },
                                {
                                    "type": {
                                        "type": "array",
                                        "items": "string"
                                    },
                                    "name": "#debunk.yaml/config/numericHeader"
                                },
                                {
                                    "type": "#debunk.yaml/quality_measure",
                                    "name": "#debunk.yaml/config/quality_measure"
                                },
                                {
                                    "type": "int",
                                    "name": "#debunk.yaml/config/threshold_individuals"
                                },
                                {
                                    "type": "int",
                                    "name": "#debunk.yaml/config/threshold_objects"
                                },
                                {
                                    "type": "float",
                                    "name": "#debunk.yaml/config/threshold_quality"
                                }
                            ]
                        }
                    ],
                    "class": "SchemaDefRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "#debunk.yaml/config",
                    "id": "#main/config"
                },
                {
                    "type": "string",
                    "id": "#main/dataset"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "Any"
                    },
                    "id": "#main/expected-results"
                }
            ],
            "outputs": [
                {
                    "type": "boolean",
                    "outputSource": "#main/compare/identical",
                    "id": "#main/identical"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "Any"
                    },
                    "outputSource": "#main/compare/only-in-2",
                    "id": "#main/only-in-expected"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "Any"
                    },
                    "outputSource": "#main/compare/only-in-1",
                    "id": "#main/only-in-results"
                }
            ],
            "steps": [
                {
                    "run": "#compare.cwl",
                    "in": [
                        {
                            "source": "#main/run-debunk/results",
                            "id": "#main/compare/results1"
                        },
                        {
                            "source": "#main/expected-results",
                            "id": "#main/compare/results2"
                        }
                    ],
                    "out": [
                        "#main/compare/identical",
                        "#main/compare/only-in-1",
                        "#main/compare/only-in-2"
                    ],
                    "id": "#main/compare"
                },
                {
                    "run": "#debunk_workflow_remote.cwl",
                    "in": [
                        {
                            "source": "#main/config",
                            "id": "#main/run-debunk/config"
                        },
                        {
                            "source": "#main/dataset",
                            "id": "#main/run-debunk/dataset"
                        }
                    ],
                    "out": [
                        "#main/run-debunk/results"
                    ],
                    "id": "#main/run-debunk"
                }
            ],
            "id": "#main"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "doc": "Settings file",
                    "type": "File",
                    "streamable": true,
                    "id": "#debunk_remote.cwl/settings"
                }
            ],
            "outputs": [
                {
                    "type": "Any",
                    "outputBinding": {
                        "glob": "results.json",
                        "loadContents": true,
                        "outputEval": "$(JSON.parse(self[0].contents))"
                    },
                    "id": "#debunk_remote.cwl/results"
                }
            ],
            "baseCommand": "node",
            "arguments": [
                {
                    "valueFrom": "const WebSocket = require(\"ws\")\nconst axios = require(\"axios\")\nconst fs = require(\"fs\")\n\nconst url = \"https://contentcheck.liris.cnrs.fr\"\n\nfs.readFile(\"$(inputs.settings.path)\", { encoding: \"utf8\" }, (err, settings) => {\n  const socket = new WebSocket(\"wss://contentcheck.liris.cnrs.fr/ws\")\n\n  socket.on(\"open\", () => {\n    socket.send(JSON.stringify({ data: settings }))\n  })\n\n  const onDone = ({ userID, jobID }) => {\n    axios.get(url + \"/api/results\", { params: { userID, jobID }})\n      .then(({ data }) => {\n        fs.writeFile(\"results.json\", JSON.stringify(data.results), () => {\n          socket.close()\n        })\n      })\n  }\n\n  const onFailed = err => {\n    socket.close()\n    throw err\n  }\n\n  socket.on(\"error\", error => {\n    socket.close()\n    throw error\n  })\n\n  socket.on(\"message\", msg => {\n    console.log('message !')\n    console.log(msg)\n    const data = JSON.parse(msg)\n\n    switch (data.type) {\n      case \"done\": {\n        onDone(data.data)\n        break\n      }\n      case \"failed\": {\n        onFailed(data.data)\n        break\n      }\n    }\n  })\n})\n",
                    "prefix": "-e"
                }
            ],
            "hints": [
                {
                    "dockerImageId": "debunk_remote",
                    "dockerFile": "FROM node:alpine\nRUN yarn add ws axios\n",
                    "class": "DockerRequirement"
                }
            ],
            "id": "#debunk_remote.cwl"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "types": [
                        {
                            "$import": "#debunk.yaml/quality_measure"
                        },
                        {
                            "$import": "#debunk.yaml/config"
                        }
                    ],
                    "class": "SchemaDefRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "#debunk.yaml/config",
                    "id": "#debunk_workflow_remote.cwl/config"
                },
                {
                    "type": "string",
                    "id": "#debunk_workflow_remote.cwl/dataset"
                }
            ],
            "outputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "Any"
                    },
                    "outputSource": "#debunk_workflow_remote.cwl/run-debunk/results",
                    "id": "#debunk_workflow_remote.cwl/results"
                }
            ],
            "steps": [
                {
                    "run": "#gen_config_file_for_remote.cwl",
                    "in": [
                        {
                            "source": "#debunk_workflow_remote.cwl/config",
                            "id": "#debunk_workflow_remote.cwl/make-config-file/config"
                        },
                        {
                            "source": "#debunk_workflow_remote.cwl/dataset",
                            "id": "#debunk_workflow_remote.cwl/make-config-file/dataset"
                        }
                    ],
                    "out": [
                        "#debunk_workflow_remote.cwl/make-config-file/settings"
                    ],
                    "id": "#debunk_workflow_remote.cwl/make-config-file"
                },
                {
                    "run": "#debunk_remote.cwl",
                    "in": [
                        {
                            "source": "#debunk_workflow_remote.cwl/make-config-file/settings",
                            "id": "#debunk_workflow_remote.cwl/run-debunk/settings"
                        }
                    ],
                    "out": [
                        "#debunk_workflow_remote.cwl/run-debunk/results"
                    ],
                    "id": "#debunk_workflow_remote.cwl/run-debunk"
                }
            ],
            "id": "#debunk_workflow_remote.cwl"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "types": [
                        {
                            "$import": "#debunk.yaml/quality_measure"
                        },
                        {
                            "$import": "#debunk.yaml/config"
                        }
                    ],
                    "class": "SchemaDefRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "#debunk.yaml/config",
                    "id": "#gen_config_file_for_remote.cwl/config"
                },
                {
                    "type": "string",
                    "id": "#gen_config_file_for_remote.cwl/dataset"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "settings.json"
                    },
                    "id": "#gen_config_file_for_remote.cwl/settings"
                }
            ],
            "hints": [
                {
                    "dockerPull": "python:3-alpine",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "python",
            "arguments": [
                {
                    "prefix": "-c",
                    "valueFrom": "import json\n\noutput = {\n  'dataset': { 'name': '$(inputs.dataset)' },\n  'delimiter': '\\\\t',\n  'nb_objects': 50000,\n  'nb_individuals': 50000,\n  'arrayHeader': $(inputs.config.arrayHeader),\n  'numericHeader': $(inputs.config.numericHeader),\n  'vector_of_outcome': None,\n  'ponderation_attribute': None,\n  'description_attributes_objects': $(inputs.config.description_attributes_objects),\n  'description_attributes_individuals': $(inputs.config.description_attributes_individuals),\n  'threshold_objects': $(inputs.config.threshold_objects),\n  'threshold_individuals': $(inputs.config.threshold_individuals),\n  'threshold_quality': $(inputs.config.threshold_quality),\n  'aggregation_measure': 'VECTOR_VALUES',\n  'similarity_measure': 'MAAD',\n  'quality_measure': '$(inputs.config.quality_measure)',\n  'algorithm': 'DSC+CLOSED+UB2',\n  'timebudget': 3600,\n  'objects_scope': [],\n  'individuals_1_scope': $(inputs.config.individuals_1_scope),\n  'individuals_2_scope': $(inputs.config.individuals_2_scope)\n}\nwith open('settings.json', 'w') as f:\n  json.dump(output, f)\n"
                }
            ],
            "id": "#gen_config_file_for_remote.cwl"
        }
    ],
    "cwlVersion": "v1.0"
}